��    �     <  _  \      �%     �%     �%     &     %&     <&  (   V&     &     �&  =   �&     �&  D   �&  	   B'     L'     Y'     k'     y'  	   �'     �'     �'     �'     �'  (   �'  $   (     +(     1(     8(     ?(     L(     Y(     k(     z(     �(     �(     �(     �(     �(     �(  
   �(     �(     �(  
   )  	   )     )     )  �   )     *     "*  #   ;*     _*     p*     �*     �*     �*     �*  &   �*  	   �*     +     +  	   +     $+     <+  
   O+     Z+  -  m+  �  �,     3.  )   F.  $   p.     �.     �.     �.     �.  	   �.  3   �.  2   */  ,   ]/  8   �/  -   �/  "   �/     0  9   30     m0  	   �0     �0  .   �0  �   �0     t1  )   �1     �1     �1  +   �1      2     ?2  "   _2     �2  	   �2  3   �2     �2  $   �2     3  !   '3  -   I3  @   w3  +   �3  0   �3  -   4     C4     K4     T4  3   s4  2   �4  
   �4  
   �4     �4  
   �4      5  .   5  %   65  &   \5  z   �5  z   �5  (   y6  "   �6     �6  $   �6     7  '   7  '   E7  &   m7     �7     �7     �7     �7     �7     8     *8     D8     V8     r8  
   v8     �8      �8     �8     �8     �8     �8     9     &9     =9     S9     r9  0   �9     �9      �9     �9  5   :     C:     _:  /  o:     �;     �;     �;     �;  @   �;  9   (<  :   b<  �  �<     P>     g>     >     �>     �>  )   �>  9   �>     *?     0?     9?     T?  
   h?  
   s?  &   ~?  (   �?     �?  	   �?     �?  
   �?  )   �?     "@  	   +@     5@  
   E@    P@  
   VA     aA     wA     �A     �A  7   �A     �A     B     B  8   7B     pB     �B     �B     �B     �B     �B     �B     C      C      ?C     `C     �C     �C     �C  #   �C  %   �C     %D  	   ED     OD  %   TD  G   zD  �   �D  �   xE     9F     IF     UF  �  mF  '   H  #   BH  (   fH  "   �H  <   �H  ;   �H  5   +I     aI     I     �I  &   �I     �I     J  #   "J     FJ     NJ     lJ  #   �J     �J  )   �J  	   �J  	   �J  &   �J  (   #K  
   LK  �  WK     M  	   M  
   M  +   *M     VM     iM     �M     �M     �M     �M     �M     �M     �M     N  *   N  >   ?N  
   ~N     �N  	   �N  	   �N     �N  	   �N  	   �N  
   �N     �N     �N  
   �N  !   �N     O      O  (   -O     VO     jO     �O     �O     �O     �O  !   �O     �O      P  &   .P  t   UP     �P     �P  $   �P  	   Q  	   Q     %Q  	   3Q  	   =Q  
   GQ  "   RQ     uQ     �Q  �   �Q  d   DR  �   �R  T   <S  �   �S  T   $T  	   yT  (   �T     �T  !   �T      �T  %   �T  )   "U     LU     hU     zU     U     �U  #   �U  3   �U     �U     V     V  (   $V     MV     dV  0   qV  #   �V  "   �V     �V  #   �V     W  !   <W  =   ^W  $   �W     �W  	   �W  %   �W  1   �W  /   /X     _X  �  fX     �Y     Z  +   Z  '   EZ     mZ  #   �Z  2   �Z     �Z     �Z     �Z  (   [     E[  �  X[     �\     ]  !   /]     Q]  
   Y]     d]  "   �]     �]  !   �]  %   �]     �]     ^  ,   ^     H^     f^  �  �^     /`     D`  >   ``  
   �`     �`  	   �`     �`     �`  &   �`     �`  )   a     :a     Wa     ta  %   �a     �a  %   �a  .   �a  h   %b  
   �b  .   �b     �b     �b     �b     �b     c     c  	   -c     7c     Gc  0   [c     �c     �c  &   �c      �c     d     *d     3d     Bd     Gd  J   ]d     �d     �d     �d  %   �d  
   �d  �  e  	   �f     �f     �f     �f     g     .g     Ig     Mg     Pg     mg     vg     �g  <   �g  
   �g  	   �g     �g     �g     �g     h     h      h  
   -h     8h     Xh  j  `h     �i  #   �i     j     $j     Bj  1   aj     �j  %   �j  @   �j     k  S   "k     vk     �k  
   �k     �k     �k  	   �k     �k  "   �k     l     l  1   -l  .   _l     �l     �l     �l     �l     �l     �l     �l     �l     �l     m     m     ,m     >m     Gm  
   Pm     [m     dm  
   mm  	   xm     �m     �m    �m     �n  #   �n  ,   �n     �n     o     &o  ,   <o  #   io     �o  +   �o  	   �o     �o     �o  
   p  %   p  !   2p     Tp     ap  m  rp  �  �q     �s  ,   �s  -   �s     t     8t  
   Vt     at     ~t  >   �t  D   �t  :   u  M   Iu  9   �u  .   �u  +    v  M   ,v  %   zv     �v     �v  0   �v  �   �v     �w  6   �w  !   x     (x  .   Fx  1   ux  +   �x  3   �x  0   y  
   8y  4   Cy     xy  +   �y  &   �y  F   �y  L   2z  V   z  A   �z  2   {  =   K{     �{     �{  )   �{  9   �{  /   �{     -|     C|     T|  
   Y|  	   d|  .   n|  %   �|  2   �|  �   �|  �   �}  <   ~     U~     s~  0   �~  #   �~  3   �~  &     2   >     q  "   �  #   �     �      �  )   �  %   ;�     a�     x�     ��     ��     ��  3   ��  ?   ހ  )   �     H�  1   g�  -   ��  +   ǁ  *   �  <   �  '   [�  V   ��  )   ڂ  @   �  ?   E�  Y   ��  9   ߃     �  K  +�     w�     ��     ��     ��  d     R   '�  F   z�  
  ��      ̈  $   �     �  ,   ,�     Y�  -   w�  @   ��     �  
   �  !   ��     �     3�     A�  8   P�  4   ��     ��  
   ˊ     ֊  	   ߊ  -   �     �     �     .�     B�  ,  P�     }�     ��     ��     ��     ֌  H   ��     9�     U�  $   q�  K   ��     �  !   �     �  %   3�     Y�  *   s�     ��  *   ��  "   �  #   �  #   *�  "   N�     q�  $   ��  !   ��  1   ԏ  (   �  
   /�     :�  /   @�  [   p�  �   ̐  �   ��     ��  
   ��     ��  �  ��  1   o�  0   ��  )   Ҕ  1   ��  Q   .�  F   ��  8   Ǖ  +    �  )   ,�  4   V�  4   ��  &   ��  &   �  2   �  	   A�     K�     i�  &   ��  !   ��  ;   ͗     	�     �  ,   &�  3   S�  
   ��  
  ��     ��  
   ��     ��  3   Ț     ��     �     0�     H�  #   g�     ��     ��     ��  	   Λ     ؛  9   �  L   '�  
   t�  
   �     ��  
   ��     ��     ��     ��     ͜     ՜     ؜     ۜ  '   �  %   �     7�  >   I�     ��  &   ��  1   ʝ     ��  !   �     .�  %   D�     j�  %   ~�  6   ��     ۞  	   [�  "   e�  (   ��     ��  
   ��     ̟     ڟ     �  	   ��  -   ��     ,�  '   =�  �   e�  t   (�  �   ��  h   T�  �   ��  h   t�     ݣ  .   �     �  .   .�  A   ]�  /   ��  /   Ϥ     ��     �     5�     <�     W�     `�  M   }�     ˥     ҥ  
   ��  .   ��  %   '�     M�  G   \�  /   ��  2   Ԧ     �  "   �  /   :�  ,   j�  J   ��  +   �     �     �  3   .�  5   b�  5   ��     Ψ  �  Ԩ     ��     ��  %   ��  1   ڪ     �  +   ,�  =   X�     ��     ��  $   ǫ  *   �     �  �  4�  *   �  .   :�  5   i�     ��  
   ��  #   ��  $   ֮     ��  )   �  1   7�     i�     m�  *   ��  !   ��  )   د  �  �     �  #   ��  L   !�     n�  	   }�     ��  
   ��     ��  A   ��     ��  1   	�     ;�     X�     u�  $   ��  %   ��  *   ޳  H   	�  t   R�     Ǵ  '   ش      �     �     "�     >�     P�     ]�     x�     ��     ��  :   ��  $   ��  -   �  <   K�  %   ��  %   ��     Զ     ܶ  
   �  '   ��  O   �     o�     u�     ��  5   ��     ַ  �  �     ƹ  "   ˹     �  /   �     >�     W�     o�     s�  4   x�     ��     ��  	   ͺ  =   ׺     �     '�      7�     X�     j�     w�     ��     ��     ��     Ż     �         �      f  �  �  =  v   8   _  �       �  �                   {   B  �   �  �   C      �      �  ~       '                     �       K   S     (       �   �   �   �       J      �  A       �   n         ^       �      �  k  )  �   �  v  �   <  �   g               X  :  �          �   �   e   b  t   ;       �   �       V  $            �   y   ?   >              3   @  �   �  �   {  �      =   �     r     >   �   �   ?  �   �       E   �  �  D   &      �                       W       �  �  �      Q  �   .   a     
      "  .      �   �      P       �       X       +   �  R  �   �   %       z      �       �   a  5   �   �  �   �      �      �   p     [      �     �      �   �           �   �   J  �  �   j   �  s       �              �       �   �  H   �   �   �  &   I  D      �            �   �  7      P                        �  �           �          �  q   �   �   p   *   !       �          O  W      Q   �   K      �   `     �      �   -           m   �       0   @   �  �   �   "       �   6  <   9   �           /   `       �       �     b     i  5      �       �      �       \   w   �  �       U      �   �  M  �   T       �   ;          t  �  �           h  �       �   �        �      �  �   y  2  +  �  �        N       u     |  q      �      �          �   �   c   O       G  [               c  k   |   9      �   w  �  -  �       �  \  '    �      �  	  �      �      �       }            U       �   o             N  H  I   Y  ]   h           �   V   �       �      C   4     G   �   F   �    �       �       �  �             E      ,  �   e      �    �       �   7   0  �      8  �   �  F  ]  �   l      /  �   j      �      �  �   �   %  3  �       x  u       	   1  !    �       �      }   �             �      �  x       �   (  �           _   �  Z  o  �   L  �  z      �    �      �          m  �           �         �   2   Z   �      B   Y   �  �   �   ,   �   g    �           f      *  #   r  �   �       �       d       s   �          ~  �          L   R   M   �   �   �  �   S   6   )   �   ^  i       �   l       �      n      �   �   
   �   �   1     #              �  d  T        �           �    �   :   A  �       4    $   
 Compiled options: 
Buffer not written to %s: %s
 
Buffer not written: %s
 
Buffer written to %s
 
Press Enter to continue
 
Press Enter to continue starting nano.
  (to replace)  (to replace) in selection  Email: nano@nano-editor.org	Web: http://www.nano-editor.org/  GNU nano, version %s
  The following function keys are available in Browser Search mode:

  [Backup]  [Backwards]  [Case Sensitive]  [DOS Format]  [Mac Format]  [Regexp] "%.*s%s" not found "%s" is a device file "%s" is a directory "%s" not found "start=" requires a corresponding "end=" %sWords: %lu  Lines: %ld  Chars: %lu (dir) (huge) (more) (parent dir) +LINE,COLUMN --backupdir=<dir> --fill=<#cols> --operatingdir=<dir> --quotestr=<str> --speller=<prog> --syntax=<str> --tabsize=<#cols> -C <dir> -Q <str> -T <#cols> -Y <str> -o <dir> -r <#cols> -s <prog> Aa All Also, pressing Esc twice and then typing a three-digit decimal number from 000 to 255 will enter the character with the corresponding value.  The following keystrokes are available in the main editor window.  Alternative keys are shown in parentheses:

 Append Append Selection to File Argument '%s' has an unterminated " At first message At last message Auto indent Auto save on exit, don't prompt Automatically indent new lines Back Background color "%s" cannot be bright Backspace Backup File Backup files Backwards Bad quote string %s: %s Bad regex "%s": %s Beg of Par Brought to you by: Browser Go To Directory Help Text

 Enter the name of the directory you would like to browse to.

 If tab completion has not been disabled, you can use the Tab key to (attempt to) automatically complete the directory name.

 The following function keys are available in Browser Go To Directory mode:

 Browser Search Command Help Text

 Enter the words or characters you would like to search for, and then press Enter.  If there is a match for the text you entered, the screen will be updated to the location of the nearest match for the search string.

 The previous search string will be shown in brackets after the search prompt.  Hitting Enter without entering any text will perform the previous search.

 Can now UnJustify! Can't go outside of %s in restricted mode Can't insert file from outside of %s Can't move up a directory Can't write outside of %s Cancel Cancel the current function Cancelled Cannot add a color command without a syntax command Cannot add a header regex without a syntax command Cannot add a linter without a syntax command Cannot add a magic string regex without a syntax command Cannot add formatter without a syntax command Cannot map name "%s" to a function Cannot map name "%s" to a menu Cannot prepend or append to a symlink with --nofollow set Cannot unset option "%s" Case Sens Close Close the current file buffer / Exit from nano Color "%s" not understood.
Valid colors are "green", "red", "blue",
"white", "yellow", "cyan", "magenta" and
"black", with the optional prefix "bright"
for foreground colors. Color syntax highlighting Command "%s" not allowed in included file Command "%s" not understood Command to execute [from %s]  Command to execute in new buffer [from %s]  Constant cursor position display Constantly show cursor position Conversion of typed tabs to spaces Convert typed tabs to spaces Copy Text Copy the current line and store it in the cutbuffer Could not create pipe Could not find syntax "%s" to extend Could not fork Could not get size of pipe buffer Couldn't determine hostname for lock file: %s Couldn't determine my identity for lock file (getpwuid() failed) Couldn't reopen stdin from keyboard, sorry
 Count the number of words, lines, and characters Creating misspelled word list, please wait... Cur Pos Cut Text Cut from cursor to end of line Cut from the cursor position to the end of the file Cut the current line and store it in the cutbuffer Cut to end CutTillEnd DIR: DOS Format Delete Delete the character to the left of the cursor Delete the character under the cursor Detect word boundaries more accurately Detected a legacy nano history file (%s) which I moved
to the preferred location (%s)
(see the nano FAQ about this change) Detected a legacy nano history file (%s) which I tried to move
to the preferred location (%s) but encountered an error: %s Directory for saving unique backup files Display the position of the cursor Display this help text Do not read the file (only write it) Do quick statusbar blanking Don't add newlines to the ends of files Don't convert files from DOS/Mac format Don't follow symbolic links, overwrite Don't hard-wrap long lines Don't look at nanorc files Don't show the two help lines Edit a replacement Enable alternate speller Enable smart home key Enable soft line wrapping Enable suspension Enable the use of the mouse End End of Par Enter Enter line number, column number Error deleting lock file %s: %s Error expanding %s: %s Error in %s on line %lu:  Error invoking "%s" Error invoking "sort -f" Error invoking "spell" Error invoking "uniq" Error opening lock file %s: %s Error reading %s: %s Error reading lock file %s: Not enough data read Error writing %s: %s Error writing backup file %s: %s Error writing lock file %s: %s Error writing lock file: Directory '%s' doesn't exist Error writing temp file: %s Execute Command Execute Command Help Text

 This mode allows you to insert the output of a command run by the shell into the current buffer (or a new buffer in multiple file buffer mode).  If you need another blank buffer, do not enter any command.

 The following function keys are available in Execute Command mode:

 Execute external command Exit Exit from nano Exit from the file browser Failed to write backup file, continue saving? (Say N if unsure)  Fatal error: no keys mapped for function "%s".  Exiting.
 File %s is being edited (by %s with %s, PID %d); continue? File Browser Help Text

 The file browser is used to visually browse the directory structure to select a file for reading or writing.  You may use the arrow keys or Page Up/Down to browse through the files, and S or Enter to choose the selected file or enter the selected directory.  To move up one level, select the directory called ".." at the top of the file list.

 The following function keys are available in the file browser:

 File Name to Append to File Name to Prepend to File Name to Write File exists, OVERWRITE ?  File to insert [from %s]  File to insert into new buffer [from %s]  File was modified since you opened it, continue saving ?  File: Finished Finished checking spelling Finished formatting First File First Line Fix Backspace/Delete confusion problem Fix numeric keypad key confusion problem For ncurses: Formatter Forward FullJstify Function '%s' does not exist in menu '%s' Get Help Go To Dir Go To Directory Go To Line Go To Line Help Text

 Enter the line number that you wish to go to and hit Enter.  If there are fewer lines of text than the number you entered, you will be brought to the last line of the file.

 The following function keys are available in Go To Line mode:

 Go To Text Go back one character Go back one word Go forward one character Go forward one word Go just beyond end of paragraph; then of next paragraph Go one screenful down Go one screenful up Go to beginning of current line Go to beginning of paragraph; then of previous paragraph Go to directory Go to end of current line Go to file browser Go to line and column number Go to next line Go to next linter msg Go to previous line Go to previous linter msg Go to the first file in the list Go to the first line of the file Go to the last file in the list Go to the last line of the file Go to the matching bracket Go to the next file in the list Go to the previous file in the list Got 0 parsable lines from command: %s Hard wrapping of overlong lines Help mode Home I can't find my home directory!  Wah! If needed, use nano with the -I option to adjust your nanorc settings.
 If you have selected text with the mark and then search to replace, only matches in the selected text will be replaced.

 The following function keys are available in Search mode:

 If you need another blank buffer, do not enter any filename, or type in a nonexistent filename at the prompt and press Enter.

 The following function keys are available in Insert File mode:

 In Selection:   Indent Text Indent the current line Insert File Help Text

 Type in the name of a file to be inserted into the current file buffer at the current cursor location.

 If you have compiled nano with multiple file buffer support, and enable multiple file buffers with the -F or --multibuffer command line flags, the Meta-F toggle, or a nanorc file, inserting a file will cause it to be loaded into a separate buffer (use Meta-< and > to switch between file buffers).   Insert a newline at the cursor position Insert a tab at the cursor position Insert another file into the current one Insert the next keystroke verbatim Internal error: can't match line %d.  Please save your work. Internal error: cannot set up redo.  Please save your work. Internal error: unknown type.  Please save your work. Invalid line or column number Invoke formatter, if available Invoke the linter, if available Invoke the spell checker, if available Invoking formatter, please wait Invoking linter, please wait Invoking spell checker, please wait Justify Justify the current paragraph Justify the entire file Key invalid in non-multibuffer mode Key name is too short Key name must begin with "^", "M", or "F" Last File Last Line Log & read location of cursor position Log & read search/replace string history Mac Format Main nano help text

 The nano editor is designed to emulate the functionality and ease-of-use of the UW Pico text editor.  There are four main sections of the editor.  The top line shows the program version, the current filename being edited, and whether or not the file has been modified.  Next is the main editor window showing the file being edited.  The status line is the third line from the bottom and shows important messages.   Mark Set Mark Text Mark Unset Mark text starting from the cursor position Missing color name Missing formatter command Missing key name Missing linter command Missing magic string name Missing option Missing regex string Missing syntax name Modified Mouse support Must specify a function to bind the key to Must specify a menu (or "all") in which to bind/unbind the key New Buffer New File Next File Next Line Next Lint Msg Next Page Next Word NextHstory Nn No No Replace No conversion from DOS/Mac format No current search pattern No file name No linter defined for this type of file! No matching bracket No more open file buffers Non-blank characters required Not a bracket Nothing in undo buffer! Nothing to re-do! Option		GNU long option		Meaning
 Option		Meaning
 Option "%s" requires an argument Option is not a valid multibyte string Path %s is not a directory and needs to be.
Nano will be unable to load or save search history or cursor positions.
 Prepend Prepend Selection to File Preserve XON (^Q) and XOFF (^S) keys Prev File Prev Line Prev Lint Msg Prev Page Prev Word PrevHstory Print version information and exit Quoting string Read %lu line Read %lu lines Read %lu line (Converted from DOS and Mac format - Warning: No write permission) Read %lu lines (Converted from DOS and Mac format - Warning: No write permission) Read %lu line (Converted from DOS and Mac format) Read %lu lines (Converted from DOS and Mac format) Read %lu line (Converted from DOS format - Warning: No write permission) Read %lu lines (Converted from DOS format - Warning: No write permission) Read %lu line (Converted from DOS format) Read %lu lines (Converted from DOS format) Read %lu line (Converted from Mac format - Warning: No write permission) Read %lu lines (Converted from Mac format - Warning: No write permission) Read %lu line (Converted from Mac format) Read %lu lines (Converted from Mac format) Read File Read a file into a new buffer by default Reading File Reading file into separate buffer Reading from stdin, ^C to abort
 Recall the next search/replace string Recall the previous search/replace string Received SIGHUP or SIGTERM
 Redid action (%s) Redo Redo the last undone operation Refresh Refresh (redraw) the current screen Regex strings must begin and end with a " character Regexp Repeat the last search Replace Replace a string or a regular expression Replace this instance? Replace with Replaced %lu occurrence Replaced %lu occurrences Requested fill size "%s" is invalid Requested tab size "%s" is invalid Restricted mode Reverse the direction of the search Save backups of existing files Save file under DIFFERENT NAME ?  Save modified buffer (ANSWERING "No" WILL DESTROY CHANGES) ?  Save modified buffer before linting? Scroll Down Scroll Up Scroll by line instead of half-screen Scroll down one line without scrolling the cursor Scroll up one line without scrolling the cursor Search Search Command Help Text

 Enter the words or characters you would like to search for, and then press Enter.  If there is a match for the text you entered, the screen will be updated to the location of the nearest match for the search string.

 The previous search string will be shown in brackets after the search prompt.  Hitting Enter without entering any text will perform the previous search.   Search Wrapped Search for a string Search for a string or a regular expression Set hard-wrapping point at column #cols Set operating directory Set width of a tab to #cols columns Silently ignore startup issues like rc file errors Smart home key Smooth scrolling Soft wrapping of overlong lines Sorry, keystroke "%s" may not be rebound Special thanks to: Spell Check Help Text

 The spell checker checks the spelling of all text in the current file.  When an unknown word is encountered, it is highlighted and a replacement can be edited.  It will then prompt to replace every instance of the given misspelled word in the current file, or, if you have selected text with the mark, in the selected text.

 The following function keys are available in Spell Check mode:

 Spell checking failed: %s Spell checking failed: %s: %s Start at line LINE, column COLUMN Suspend Suspension Switch to the next file buffer Switch to the previous file buffer Switched to %s Syntax "%s" has no color commands Syntax definition to use for coloring Tab Thank you for using nano! The "default" syntax must take no extensions The "none" syntax is reserved The Free Software Foundation The bottom two lines show the most commonly used shortcuts in the editor.

 The notation for shortcuts is as follows: Control-key sequences are notated with a caret (^) symbol and can be entered either by using the Control (Ctrl) key or pressing the Escape (Esc) key twice.  Escape-key sequences are notated with the Meta (M-) symbol and can be entered using either the Esc, Alt, or Meta key depending on your keyboard setup.   The nano text editor This is the only occurrence This message is for unopened file %s, open it in a new buffer? To Bracket To Files To Linter To Spell Toggle appending Toggle backing up of the original file Toggle prepending Toggle the case sensitivity of the search Toggle the use of DOS format Toggle the use of Mac format Toggle the use of a new buffer Toggle the use of regular expressions Too many backup files? Two single-column characters required Type '%s -h' for a list of available options.
 Unable to create directory %s: %s
It is required for saving/loading search history or cursor positions.
 Uncut Text Uncut from the cutbuffer into the current line Undid action (%s) Undo Undo the last operation Unicode Input Unindent Text Unindent the current line Unjustify Unknown Command Unknown option "%s" Usage: nano [OPTIONS] [[+LINE,COLUMN] FILE]...

 Use "fg" to return to nano.
 Use (vim-style) lock files Use bold instead of reverse video text Use of one more line for editing Use one more line for editing Verbatim Verbatim Input View View mode (read-only) Warning: Modifying a file which is not locked, check directory permission? Where Is WhereIs Next Whitespace display Window size is too small for nano...
 Word Count Write File Help Text

 Type the name that you wish to save the current file as and press Enter to save the file.

 If you have selected text with the mark, you will be prompted to save only the selected portion to a separate file.  To reduce the chance of overwriting the current file with just a portion of it, the current filename is not the default in this mode.

 The following function keys are available in Write File mode:

 Write Out Write Selection to File Write the current file to disk Wrote %lu line Wrote %lu lines XOFF ignored, mumble mumble XON ignored, mumble mumble Yes Yy and anyone else we forgot... disabled enable/disable enabled line %ld/%ld (%d%%), col %lu/%lu (%d%%), char %lu/%lu (%d%%) line break line join nano is out of memory! text add text cut text delete text insert text replace text uncut the many translators and the TP version Project-Id-Version: nano 2.4.2
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-06 00:22-0500
PO-Revision-Date: 2015-08-03 20:52+0200
Last-Translator: Jordi Mallach <jordi@gnu.org>
Language-Team: Catalan <ca@dodds.net>
Language: ca
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=n!=1;
 
 Opcions compilades: 
No s'ha escrit el búfer a %s: %s
 
No s'ha escrit el búfer: %s
 
S'ha escrit el búfer en %s
 
Premeu Intro per a continuar
 
Premeu Intro per a continuar carregant el nano.
  (a reemplaçar)  (a reemplaçar) dins de la selecció  Correu-e: nano@nano-editor.org	Web: http://www.nano-editor.org/  GNU nano, versió %s
  Les tecles de funció següents són disponibles al mode de cerca del navegador:

  [Còpia de seguretat]  [Cap Enrere]  [Maj/Min]  [Format DOS]  [Format Mac]  [ExpReg] «%.*s%s» no trobat «%s» és un fitxer de dispositiu «%s» és un directori «%s» no trobat «start=» requereix el seu «end=» corresponent %sParaules: %lu  Línies: %ld  Caràcters: %lu (dir) (enorme) (més) (directori pare) +LÍNIA,COLUMNA --backupdir=<dir> --fill=<#cols> --operatingdir=<dir> --quotestr=<cad> --speller=<prog> --syntax=<cad> --tabsize=<#cols> -C <dir> -Q <str> -T <#cols> -Y <cad> -o <dir> -r <#cols> -s <prog> TtAa Totes També, prement Esc dos vegades i després introduint un número de tres xifres del 000 al 255 introduirà el caràcter amb el valor corresponent. Les següents combinacions estan disponibles a la finestra principal. Les tecles alternatives estan representades entre parèntesi:

 Afegeix Afegeix la sel·lecció a un fitxer L'argument «%s» té una «"» no terminada Al primer missatge A l'últim missatge Sagna automàticament Desa automàticament en sortir, no preguntes Sagna automàticament noves línies Enrere El color de fons «%s» no pot ser brillant Retrocés Fes còpia de seguretat Fitxers de còpia de seguretat Cap Enrere El marcador de cita %s és erroni: %s L'expreg «%s» és invàlida: %s Princ de par Per cortesia de: Text d'ajuda d'«anar a un directori» del navegador

 Introduïu el nom del directori pel qual voleu navegar.

 Si el completat amb el tabulador no està desactivat, podeu fer servir la tecla Tab per a (intentar) completar automàticament el nom del directori.

 Les següents tecles de funció estan disponibles en el mode del navegador «anar a un directori»:

 Text d'ajuda per a l'ordre Cerca d'ordres

 Introduïu les paraules o caràcters que voleu cercar i premeu Retorn.  Si hi ha una coincidència per a el text que heu introduït, la pantalla s'actualitzarà al lloc on està la coincidència més propera de la cadena cercada.

 La cadena de la recerca anterior es mostrarà entre claudàtors després de l'indicatiu de recerca. Prémer Retorn sense introduir cap text durà a terme l'anterior recerca.

 Ara podeu desjustificar! No es pot anar fora de %s en mode restringit No es poden inserir fitxers des de fora de %s No es pot ascendir de directori No es pot escriure fora de %s Cancel·la Cancel·la la funció actual Cancel·lat No es pot afegir una ordre de color sense una ordre de sintaxi No es pot afegir una expreg de capçalera sense una ordre de sintaxi No es pot afegir un analitzador sense una ordre de sintaxi No es pot afegir una expreg de cadena de «magic» sense una ordre de sintaxi No es pot afegir un formatador sense una ordre de sintaxi No es pot assignar el nom «%s» a una funció No es pot assignar el nom «%s» a un menú No es pot avantposar o afegir a un enllaç simbòlic amb --nofollow establert No es pot desestablir l'opció «%s» Maj/Min Tanca Tanca el búfer de fitxer actual / Surt del nano No s'ha reconegut el color «%s».
Els colors vàlids són «green», «red», «blue»,
«white», «yellow», «cyan», «magenta» i
«black», amb el prefix opcional «bright»
per als colors de primer pla. Resaltat de sintaxi en color L'ordre «%s» no es pot utilitzar a un fitxer inclós No es pot entendre l'ordre «%s» Ordre a executar [des de %s]  Fitxer a executar a un nou búfer [des de %s]  Visualització constant de la posició del cursor Mostra contínuament la posició del cursor Conversió de les tabulacions introduïdes a espais Converteix les tabulacions introduïdes a espais Copia text Copia la línia actual i emmagatzema-la al cutbuffer No s'ha pogut crear un conducte No s'ha trobat la sintaxi «%s» a estendre No s'ha pogut iniciar un altre procés No s'ha pogut obtindre la mida de la memòria intermèdia del conducte No s'ha pogut determinar el nom de l'ordinador per al fitxer de blocatge: %s No s'ha pogut determinar la identitat per al fitxer de blocatge (ha fallat getpwuid()) No s'ha pogut tornar a obrir l'entrada estàndard des del teclat
 Compta el nombre de paraules, línies i caràcters S'està creant una llista de paraules incorrectes, espereu... Pos Act Retalla Retalla des del cursor al final de línia Retalla des de la posició del cursor al final del fitxer Retalla la línia actual i desa-la al cutbuffer Retalla fins al final RetallaFinsFinal DIR: Format DOS Suprimeix Suprimeix el caràcter a l'esquerra del cursor Suprimeix el caràcter sota el cursor Detecta els límits entre paraules amb més detall S'ha detectat un fitxer d'historial antic (%s) que s'ha mogut
al lloc aconsellat (%s)
(Vegeu les PMF del nano per a obtenir més informació sobre aquest canvi) S'ha detectat un fitxer d'historial antic (%s) que s'ha intentat moure
al lloc aconsellat (%s), però s'ha produït un error: %s Directori on desar els fitxers de còpia de seguretat únics Mostra la posició del cursor Mostra aquest text d'ajuda No llegeixes el fitxer (només escriu sobre ell) Neteja la barra d'estat ràpidament No afegeixes retorns de carro al final dels fitxers No converteixes des del format DOS/Mac No segueixes enllaços simbòlics, sobreescriu-los No ajustes les línies llargues No faces servir els fitxers nanorc No mostres les dues línies d'ajuda Edita un reemplaçament Habilita un corrector alternatiu Habilita la tecla «inici» intel·ligent Habilita l'ajustament suau de línies Habilita la suspensió Habilita l'ús del ratolí Fi Final de par Retorn Introduïu el número de línia, número de columna S'ha produït un error en suprimir el fitxer de blocatge %s: %s S'ha produït un error en expandir %s: %s Error en %s en la línia %lu:  S'ha produït un error en iniciar executar «%s» S'ha produït un error en iniciar «sort -f» S'ha produït un error en iniciar «spell» S'ha produït un error en iniciar «uniq» S'ha produït un error en obrir el fitxer de blocatge %s: %s S'ha produït un error en llegir %s: %s S'ha produït un error en llegir el fitxer de blocatge %s: No s'han llegit prous dades S'ha produït un error en escriure %s: %s S'ha produït un error en escriure la còpia de seguretat %s: %s S'ha produït un error en escriure el fitxer de blocatge %s: %s S'ha produït un error en escriure el fitxer de blocatge: el directori «%s» no existeix S'ha produït un error en escriure un fitxer temporal: %s Executa una ordre Text d'ajuda d'Executa una ordre

 Aquest mode us permet inserir l'eixida d'una ordre executada per l'intèrpret al búfer actual (o un nou búfer al mode múltiples búfers de fitxer). Si necessiteu un altre búfer en blanc, no introduïu cap ordre.

 Les següents tecles de funció estan disponibles al mode Executa una ordre:

 Executa una ordre externa Surt Surt del nano Surt del navegador de fitxers No s'ha pogut escriure la còpia de seguretat, voleu continuar desant (Digueu N si no n'esteu segur) Error fatal: no hi ha lletres asignades per a la funció «%s». S'està sortint.
 El fitxer %s s'està editant (per %s amb %s, PID %d); voleu continuar? Text d'ajuda del navegador de fitxers

 El navegador de fitxers s'utilitza per a navegar visualment l'estructura del directori per a seleccionar un fitxer per a lectura o escriptura. Podeu fer servir els cursors o Re/Av Pag per a navegar per els fitxers i S o Intro per a triar el fitxer seleccionat o entrar dins del directori seleccionat. Per a pujar un nivell, seleccioneu el directori «..» en la part superior de la llista de fitxers.

 Les següents tecles de funció estan disponibles en el navegador de fitxers:

 Nom del fitxer en el qual afegir Nom del fitxer en el qual avantposar Nom del fitxer a escriure El fitxer existeix, el voleu SOBREESCRIURE?  Fitxer a inserir [des de %s]  Fitxer a inserir a un nou búfer [des de %s]  S'ha modificat el fitxer des de que l'heu obert, voleu desar-lo? Fitxer: Finalitzat Revisió d'ortografia finalitzada Ha finalitzat el format Primer fitxer Primera línia Arregla el problema de confusió entre Esborra/Suprimeix Arregla el problema de confusió del teclat numèric Per ncurses: Formatador Endavant JustCompl La funció «%s» no existeix al menú «%s» Ajuda Vés a directori Vés a un directori Vés a línia Text d'ajuda d'anar a línia

 Introduïu el número de la línia a la qual voleu anar i premeu Retorn. Si hi ha menys línias de text que el número que heu introduit, el cursor es mourà a la última línia del fitxer.

 Les següents tecles de funció estan disponibles en el mode anar a línia:

 Vés al text Vés enrere un caràcter Vés enrere una paraula Vés endavant un caràcter Vés endavant una paraula Vés al final del paràgraf actual; posteriorment del paràgraf següent Vés a la pantalla següent Vés a la pantalla anterior Vés al principi de la línia actual Vés al principi del paràgraf actual; posteriorment del paràgraf anterior Vés al directori Vés al final de la línia actual Vés al navegador de fitxers Vés a un número de línia i columna Vés a la línia següent Vés al missatge següent de l'analitzador Vés a la línia anterior Vés al missatge anterior de l'analitzador Vés al primer fitxer de la llista Vés a la primera línia del fitxer Vés a l'últim fitxer de la llista Vés a l'última línia del fitxer Vés a la clau corresponent Vés al fitxer següent de la llista Vés al fitxer previ de la llista S'han rebut 0 línies analitzables de l'ordre: %s Ajusta completament les línies llargues Mode ajuda Inici No s'ha pogut trobar el directori d'usuari! Ai! Si és necessari, empreu el nano amb l'opció -I  per a ajustar les vostres preferències.
  Si heu seleccionat text amb la marca i després feu una recerca amb reemplaç, només les coincidències al text seleccionat seran reemplaçades.

 Les següents tecles de funció estan disponibles en el mode Cerca:

  Si necessiteu un altre búfer en blanc, no inseriu cap nom de fitxer o escriviu el nom d'un fitxer no existent en l'indicatiu i premeu Retorn.

 Les següents tecles de funció estan disponibles en el mode insereix fitxer:

 A la selecció:   Sagna text Ajusta el fitxer actual Text d'ajuda d'insereix fitxer

 Escriviu el nom del fitxer a afegir en el búfer actual en la posició actual del cursor.

 Si s'ha compilat nano amb suport per a múltiples fitxers i heu habilitat els búfers múltiples amb les opcions -F o --multibuffer, l'interruptor Meta-F o amb un fitxer nanorc, la inserció d'un fitxer farà que es carregue en un búfer diferent (feu servir Meta-< i > per a canviar de búfers de fitxer).  Insereix una línia nova a la posició del cursor Insereix una tabulació a la posició del cursor Insereix un altre fitxer dins de l'actual Insereix la següent premuda de tecla textualment Error intern: no hi ha coincidència per a la línia %d. Deseu el vostre treball. Error intern: no es pot configurar el desfer. Deseu el vostre treball. Error intern: tipus desconegut. Deseu el vostre treball. El número de línia o columna és invàlid Invoca el formatador, si està disponible Invoca l'analitzador sintàctic, si està disponible Invoca el corrector ortogràfic, si està disponible S'esta invocant el formatador, espereu S'esta invocant l'analitzador, espereu S'està invocant el corrector ortogràfic, espereu Justifica Justifica el paràgraf actual Justifica el fitxer sencer Tecla il·legal en mode no-multibuffer El nom de la tecla és massa curt El nom de la tecla ha de començar amb «^», «M» o «F» Últim fitxer Última línia Emmagatzema i llegeix la posició del cursor Registra i llegeix la història de cerca/reemplaça Format Mac Text d'ajuda de nano

 L'editor nano està dissenyat per a emular la funcionalitat i la facilitat d'ús del Pico, l'editor de text de la UW. Hi ha quatre seccions a l'editor: la línia superior mostra la versió del programa, el nom del fitxer editat i si el fitxer ha estat o no modificat. La finestra principal d'edició  mostra el fitxer que s'està editant. La línia d'estat és la tercera per la part inferior i mostra missatges importants. Les últimes dues línies mostren les dreceres més utilitzades a l'editor. Marca establida Marca text Marca esborrada Marca text començant des de la posició del cursor Manca el nom del color Manca l'ordre del formatador Manca el nom de la clau Manca l'ordre de l'analitzador Manca el nom de cadena de «magic» Manca l'opció Manca la cadena d'expreg Manca el nom de la sintaxi Modificat Suport per a ratolí Heu d'especificar una funció a la qual assignar la tecla Heu d'especificar un menú (o «all») en què assignar/desassignar la tecla Nou búfer Nou fitxer Fitxer següent Seg Línia Miss lint seg Pàg Seg Paraula següent HistSeg Nn No No reemplaces Sense conversió des del format DOS/Mac No hi ha cap patró de recerca actual Cap nom de fitxer No hi ha cap analitzador definit per a aquest tipus de fitxer! No hi ha clau corresponent No hi ha més búfers de fitxer oberts Es requereixen caràcters que no siguen d'espaiat No és una clau No hi ha res al búfer de desfer. No hi ha res a refer. Opció		Opció llarga GNU	Significat
 Opció		Significat
 L'opció «%s» requereix un argument L'opció no és una cadena d'octets múltiples vàlida El camí %s no és un directori i ho ha de ser.
Nano no podrà carregar o desar l'historial de cerca o la posició del cursor.
 Avantposa Avantposa sel·lecció a un fitxer Preserva les tecles XON (^Q) i XOFF (^S) Fitxer anterior Ant Línia Miss lint ant Pàg Ant Paraula anterior HistPrèv Imprimeix informació sobre la versió i surt Marcador de cita %lu línia llegida %lu línies llegides %lu línia llegida (convertida des del format DOS i Mac - Avís: no hi ha permís d'escriptura) %lu línies llegides (convertides des del format DOS i Mac - Avís: no hi ha permís d'escriptura) %lu línia llegida (convertida des del format DOS i Mac) %lu línies llegides (convertides des del format DOS i Mac) %lu línia llegida (convertida des del format DOS - Avís: no hi ha permís d'escriptura) %lu línies llegides (convertides des del format DOS - Avís: no hi ha permís d'escriptura) %lu línia llegida (convertida des del format DOS) %lu línies llegides (convertides des del format DOS) %lu línia llegida (convertida des del format Mac - Avís: no hi ha permís d'escriptura) %lu línies llegides (convertides des del format Mac - Avís: no hi ha permís d'escriptura) %lu línia llegida (convertida des del format Mac) %lu línies llegides (convertides des del format Mac) Llegeix Llegeix un fitxer en un búfer nou per defecte S'està llegint el fitxer S'està llegint el fitxer en un búfer distint S'està llegint des de l'entrada estàndard, ^C per a avortar-ho
 Recupera la cadena de cerca/reemplaça següent Recupera la cadena de cerca/reemplaça anterior S'ha rebut SIGHUP o SIGTERM
 S'ha refet l'acció (%s) Refés Refés l'última operació Refresca Redibuixa la pantalla actual Les cadenes d'expressió regular han de començar i acabar amb un caràcter " Expreg Repeteix l'última recerca Reemplaça Reemplaça una cadena o una expressió regular Voleu reemplaçar aquesta instància? Reemplaça amb S'ha reemplaçat %lu coincidència S'han reemplaçat %lu coincidències La mida de plenat demanada «%s» és invàlida La mida de tabulador demanada «%s» és invàlida Mode restringit Inverteix la direcció de la cerca Fes còpies de seguretat dels fitxers existents Voleu desar el fitxer sota un NOM DIFERENT?  Voleu desar el búfer modificat (RESPONDRE «No» DESTRUIRÀ ELS CANVIS) ? Voleu desar el búfer abans d'analitzar-ho? Desplaça avall Desplaça amunt Desplaça't per línies en lloc de mitges pantalles Desplaça avall una línia sense desplaçar el cursor Desplaça amunt una línia sense desplaçar el cursor Cerca Text d'ajuda per a l'ordre Cerca

 Introduïu les paraules o caràcters que voleu cercar i premeu Retorn.  Si hi ha una coincidència per a el text que heu introduït, la pantalla s'actualitzarà al lloc on està la coincidència més propera de la cadena cercada.

 La cadena de la recerca anterior es mostrarà entre claudàtors després de l'indicatiu de recerca. Prémer Retorn sense introduir cap text durà a terme l'anterior recerca.  Recerca recomençada Cerca una cadena Cerca una cadena o expressió regular Estableix el punt d'ajustament a la columna #cols Estableix directori d'operació Estableix l'amplada de tab a #cols columnes Omet silenciosament problemes d'inici com errors al fitxer rc Tecla «Inici» intel·ligent Desplaçament suau Ajusta suaument les línies llargues La combinació «%s» no es pot reassignar Agraïments especials per a: Text d'ajuda del corrector d'ortografia

 El corrector d'ortografia comprova l'ortografia de tot el text al fitxer actual. Quan es troba una paraula desconeguda, aquesta es ressalta i es pot editar una substitució. Després preguntarà si es vol reemplaçar totes les coincidències d'eixa paraula mal escrita al fitxer actual, o, si heu seleccionat text amb la marca, al text marcat.

 Les següents tecles de funció estan disponibles en el mode corrector d'ortografia:

 La comprovació d'ortografia ha fallat: %s La comprovació d'ortografia ha fallat: %s: %s Comença en la línia número LÍNIA, columna COLUMNA Suspèn Suspensió Canvia al següent búfer de fitxer Canvia a l'anterior búfer de fitxer S'ha canviat a %s La sintaxi «%s» no té ordres de colors Definició de sintaxi a utilitzar per a colorejar Tab Gràcies per fer servir nano! La sintaxi «default» no admet extensions La sintaxi «none» és reservada La Fundació per al Software Lliure (FSF) Les dos línies inferiors mostren les dreceres més usades a l'editor.

 La notació de les dreceres és la següent: les sequències amb la tecla Control estan anotades amb el símbol circumflex (^) i s'introdueixen mitjançant la tecla Control o prement la tecla Escape (Esc) dos vegades. Les seqüències amb tecles d'escapada estan anotades amb el símbol Meta (M) i s'hi pot accedir mitjançant les tecles Esc, Alt o Meta, tot depenent de la configuració del vostre teclat.  L'editor de text GNU nano Aquesta és la única coincidència Aquest missatge és pel fitxer no obert %s, voleu obrir-lo en un nou búfer? Vés a la clau A fitxers Analitzador Ortografia Commuta l'afegiment Commuta la creació d'una còpia de seguretat del fitxer original Commuta l'avantposat Commuta la sensibilitat a majúscules de la cerca Commuta l'ús del format DOS Commuta l'ús del format Mac Commuta l'ús d'un búfer nou Commuta l'ús d'expressions regulars Massa fitxers de còpia de seguretat? Es necessiten dos caràcters d'una columna Introduïu «%s -h» per obtenir una llista de les opcions disponibles.
 No s'ha pogut crear el directori %s: %s
És requerit per desar/carregar l'historial de cerca o posició del cursor.
 Destalla el text Enganxa el cutbuffer a la línia actual S'ha desfet l'acció (%s) Desfés Desfés l'última operació Entrada d'Unicode Desagna text Desajusta el fitxer actual Desjustifica L'ordre és desconeguda L'opció «%s» és desconeguda Forma d'ús: nano [OPCIONS] [[+LÍNIA,COLUMNA] FITXER]...
 Empreu «fg» per a tornar al nano.
 Empra fitxers de blocatge (a l'estil del Vim) Utilitza negreta en comptes de text amb els colors invertits Utilitza una línia més per a editar Utilitza una línia més per a editar Textual Entrada textual Visualitza Mode de visualització (només lectura) Avís: s'està modificant un fitxer no blocat, comproveu permisos del directori Cerca On és la següent Mostra l'espai en blanc La mida del terminal és massa petita per al nano...
 Compte de paraules Text d'ajuda de desa el fitxer

 Escriviu el nom amb el que voleu desar el fitxer actual i premeu Retorn per a desar-ho.

 Si heu seleccionat text amb la marca, s'us preguntarà si voleu desar només la porció marcada a un fitxer diferent. Per a reduir la possibilitat de sobreescriure el fitxer actual amb només una part d'ell, el nom del fitxer actual no és el predeterminat en aquest mode.

 Les següents tecles de funció estan disponibles en el mode desa el fitxer:

 Desa Escriu la sel·lecció a un fitxer Escriu el fitxer actual al disc S'ha escrit %lu línia S'han escrit %lu línies XOFF ignorat, blah, blah XON ignorat, blah, blah Sí SsYy i per qualsevol dels qui ens hem oblidat esmentar... inhabilitat habilita/inhabilita habilitat línia %ld/%ld (%d%%), col %lu/%lu (%d%%), car %lu/%lu (%d%%) trencat de línia unió de línia nano s'ha quedat sense memòria! afegiment de text tall de text supressió de text inserció de text reemplaçament de text enganxada de text els molts traductors i el TP versió 