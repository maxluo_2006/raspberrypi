��    �     $  W  ,      �%     �%     �%     �%     �%     �%  (   &     /&     =&  =   X&     �&  D   �&  	   �&     �&     	'     '     )'  	   7'     A'     T'     j'     ~'  (   �'  $   �'     �'     �'     �'     �'     (     (     #(     8(     I(     Z(     i(     {(     �(  
   �(     �(     �(  
   �(  	   �(     �(     �(  �   �(     �)     �)  #   �)     *     *     )*     5*     U*     t*  &   y*  	   �*     �*     �*  	   �*     �*     �*  
   �*     +  -  +  �  D,     �-  )   �-  $   .     >.     X.     r.     y.  	   �.  3   �.  2   �.  ,   /  8   3/  -   l/  "   �/     �/  9   �/     0  	   /0     90  .   ?0  �   n0     1  )   71     a1     }1  +   �1      �1     �1  "   2     +2  	   H2  3   R2     �2  $   �2     �2  !   �2  -   �2  @    3  +   a3  0   �3  -   �3     �3     �3     �3  3   4  2   P4  
   �4  
   �4     �4  
   �4     �4  .   �4  %   �4  &   5  z   ,5  z   �5  (   "6  "   K6     n6  $   �6     �6  '   �6  '   �6  &   7     =7     X7     s7     �7     �7     �7     �7     �7     �7     8  
   8     *8      08     Q8     q8     �8     �8     �8     �8     �8     �8     9  0   09     a9      v9     �9  5   �9     �9     :  /  :     H;     a;     f;     u;  @   �;  9   �;  :   <  �  F<     �=     >     (>     ;>     U>  )   o>  9   �>     �>     �>     �>     �>  
   ?  
   ?  &   '?  (   N?     w?  	   �?     �?  
   �?  )   �?     �?  	   �?     �?  
   �?    �?  
   �@     
A      A     1A     JA  7   ^A     �A     �A     �A  8   �A     B     )B     CB     VB     sB     �B     �B     �B      �B      �B     	C     )C     IC     dC  #   �C  %   �C     �C  	   �C     �C  %   �C  G   #D  �   kD  �   !E     �E     �E     �E  �  F  '   �G  #   �G  (   H  "   8H  <   [H  ;   �H  5   �H     
I     (I     GI  &   gI     �I     �I  #   �I     �I     �I     J  #   -J     QJ  )   gJ  	   �J  	   �J  &   �J  (   �J  
   �J  �   K     �L  	   �L  
   �L  +   �L     �L     M     ,M     =M     TM     nM     }M     �M     �M     �M  *   �M  >   �M  
   'N     2N  	   ;N  	   EN     ON  	   ]N  	   gN  
   qN     |N     N  
   �N  !   �N     �N     �N  (   �N     �N     O     -O     KO     YO     qO  !   �O     �O      �O  &   �O  t   �O     sP     {P  $   �P  	   �P  	   �P     �P  	   �P  	   �P  
   �P  "   �P     Q     -Q  �   JQ  d   �Q  �   RR  T   �R  �   :S  T   �S  	   "T     ,T      9T  %   ZT  )   �T     �T     �T     �T     �T     �T  #   U  3   (U     \U     cU     zU  (   �U     �U     �U  0   �U  #    V  "   $V     GV  #   WV     {V  !   �V  =   �V  $   �V     W  	   +W  %   5W  1   [W  /   �W     �W  �  �W     TY     cY  +   wY  '   �Y     �Y  #   �Y  2   Z     :Z     IZ     ZZ  (   zZ     �Z  �  �Z     U\     o\  !   �\     �\  
   �\     �\  "   �\     ]  !   ]  %   5]     []     _]  ,   y]     �]     �]  �  �]     �_     �_  >   �_  
   �_     `  	   `     `     $`  &   5`     \`  )   n`     �`     �`     �`  %   �`     a  %   .a  .   Ta  h   �a  
   �a  .   �a     &b     8b     =b     Ub     cb     qb  	   �b     �b     �b  0   �b     �b     c  &   "c      Ic     jc     �c     �c     �c     �c  J   �c     d     d     d  %   /d  
   Ud  �  `d  	   f     f     2f     Qf     pf     �f     �f     �f     �f     �f     �f     �f  <   �f  
   (g  	   3g     =g     Tg     ]g     fg     rg     ~g  
   �g     �g     �g  �  �g      �i  ;   �i  6   �i  4   0j  9   ej  W   �j     �j  $   k  K   1k  &   }k  m   �k     l     $l  (   4l     ]l     rl     �l  "   �l  -   �l  &   �l     m  B   4m  7   wm     �m     �m     �m     �m     �m     n     n     0n     Hn     cn     yn     �n     �n     �n     �n     �n     �n     �n     o     o  �  o     �p  6   �p  F   -q     tq  "   �q     �q  2   �q  7   �q  
   6r  F   Ar     �r     �r  !   �r     �r  0   �r  1    s     Rs     ls  �  �s  �  zu  .   x  R   1x  @   �x  ?   �x  '   y     -y  0   :y     ky  \   |y  s   �y  S   Mz  |   �z  Y   {  K   x{  M   �{  |   |  =   �|     �|     �|  f   �|  �  V}  -   �~  Z   !  1   |  9   �  `   �  9   I�  E   ��  L   ɀ  F   �     ]�  C   u�  0   ��  I   �  #   4�  X   X�  n   ��  �    �  w   ��  4   &�  W   [�     ��     ф  7   �  R   �  C   r�     ��     Յ     ��     ��     �  0   �  +   I�  5   u�  �   ��  �   ��  c   h�  0   ̈  1   ��  7   /�  :   g�  L   ��  P   �  H   @�  2   ��  4   ��  4   �     &�  C   D�  A   ��  9   ʋ  !   �  ,   &�     S�     \�  
   r�  7   }�  P   ��  &   �  %   -�  2   S�  7   ��  5   ��  4   �  R   )�  -   |�  �   ��  7   ,�  N   d�  V   ��  x   
�  C   ��     ǐ  =  �  ,   !�  
   N�     Y�  7   t�  �   ��  u   a�  q   ה  D  I�  ?   ��  @   Θ  =   �  9   M�  /   ��  T   ��  ~   �     ��     ��  7   ��  *   �     �     +�  [   ;�  V   ��     �     �     �     &�  L   B�     ��     ��  "   ��     �  �  �     ��  '   ��  %   ў  )   ��  '   !�  ^   I�  "   ��     ˟  +   �  N   �  "   e�  %   ��  -   ��  $   ܠ     �  6   �     S�  :   r�  5   ��  *   �  =   �  2   L�  ,   �  ;   ��  :   �  D   #�  B   h�     ��     ã  b   ң  �   5�  4  Ѥ  h  �     o�     ��      ��     ��  =   ��  @   ��  6   7�  N   n�  ~   ��  �   <�  m   ��  :   /�  A   j�  ;   ��  N   �  B   7�  <   z�  O   ��     �  *   �  0   E�  ]   v�  1   ԯ  C   �     J�     j�  C   ��  T   ư     �    7�  !   ?�     a�     y�  O   ��  &   �  6   �  ,   G�  0   t�  7   ��     ݵ  ?   ��  .   =�     l�     ~�  V   ��  z   �  !   m�     ��     ��     ɷ     ߷     ��     �     ,�     I�     P�     U�  E   e�  5   ��  &   �  M   �  0   V�  N   ��  2   ֹ     	�  I   !�  2   k�  @   ��     ߺ  5   ��  D   3�  �   x�     o�  <   ��  3   ļ  #   ��     �     6�     Q�     q�     ��  ?   ��      �  O   	�  �  Y�    =�  �  [�  �   �  �  ��  �   ��     ��  !   ��  N   ��  I   8�  M   ��  #   ��  1   ��     &�  9   5�     o�  H   |�  n   ��     4�  0   G�     x�  @   ��  (   ��     ��  _   �  I   a�  Q   ��     ��  .   �  =   L�  Q   ��     ��  T   \�     ��     ��  =   ��  S   �  S   s�  
   ��  p  ��     C�     Z�  :   p�  Z   ��  4   �  Z   ;�  }   ��  0   �     E�  D   a�  g   ��  +   �  /  :�  ;   j�  ?   ��  :   ��     !�     2�  U   C�  Y   ��     ��  :   �  ;   J�     ��  5   ��  O   ��  7   �  6   W�  �  ��  &   :�  %   a�  �   ��     �     *�     >�      R�  .   s�  R   ��  *   ��  W    �  9   x�  C   ��  G   ��  G   >�  3   ��  A   ��  P   ��  �   M�     !�  2   :�  9   m�     ��  .   ��     ��     ��  ;   �     Y�  !   j�  (   ��  \   ��  B   �  J   U�  W   ��  ?   ��  ?   8�     x�     ��     ��  8   ��  �   ��     ��     ��     ��  E   ��     �  '  &�     N�  -   [�  :   ��  O   ��  -   �  ,   B�     o�     t�  F   }�     ��  '   ��     ��  K   �     \�     n�  4   ��     ��     ��     ��     �     '�  2   A�  0   t�     ��     �       Y    
      ?  �         x   J   T  �      �  �          �    �  �   �  �  �         g     �   J  Z   �       �       '  >   -   v   �       �             n   �  �     �           �                  �  �   *  �   �   �   �          �   �   �           `            U  )   E  �       �   �   �  i     �   -  �  ~  �      �       �  �   �     �    �   k  �   �  �  �   
       D   R   �   �      �  "  {   �  �   '       �   �   m   #      O  L          <   u               �               �   *   M  �      �   /  m  z   �  B  K  (   3       �      t                   �   H      %  �   �  	   �   W  �   n     I   a   �  �   3          8  &      y   2   ^  \  D      .       �   �  A           �   �       N  �           �    �   �   �  s  �  M       �       Z  �       �  d  ]      R      |   P         �   �       �   j       �   �       :   �         �          �  �   �     i             �   o          �     �       u    �   �   �      �   �   #   �  �   f  O   9       �   �   �       �       y  r   �  �              �  k   �      v              �       �       �  �   �   j      +  G   �   �   �  A  p   a      "       `      0       %   5   �  �  w       !  �       �   z  V                �       �             L      4  7   �           �         6   @  T   X  �  7  �   �      2  �                   �   C   Q  ]   I     (        �      o   c  �          �         �  �   [   �   P      �   f   �   d       x     q  9  1  h   �               p      �               >  �  6            4   �       l   )      V   �   �   @       �   �  &  8   �   {  }   �   �   �   |  X   �   C  \   _   �       �   ^   S   �   �  N          �  �       .      5  �               �   �   �     Y   �      ;  �   ?   �   �  !   q   =     S              �   H  e   �   U       <  e  F       	  B         r    }  1   ,   =   s   g                 �  �   �  E      ;   �     �   �   G     t          �           �  :  �  K   0      +       �      �   �       W   _      Q     h    �   �       �  ,  �   �  b  $           �  �  �  c   ~   l  �           b          �   w  �       $  F      /   �      �         �  �         �          �     [   
 Compiled options: 
Buffer not written to %s: %s
 
Buffer not written: %s
 
Buffer written to %s
 
Press Enter to continue
 
Press Enter to continue starting nano.
  (to replace)  (to replace) in selection  Email: nano@nano-editor.org	Web: http://www.nano-editor.org/  GNU nano, version %s
  The following function keys are available in Browser Search mode:

  [Backup]  [Backwards]  [Case Sensitive]  [DOS Format]  [Mac Format]  [Regexp] "%.*s%s" not found "%s" is a device file "%s" is a directory "%s" not found "start=" requires a corresponding "end=" %sWords: %lu  Lines: %ld  Chars: %lu (dir) (more) (parent dir) +LINE,COLUMN --backupdir=<dir> --fill=<#cols> --operatingdir=<dir> --quotestr=<str> --speller=<prog> --syntax=<str> --tabsize=<#cols> -C <dir> -Q <str> -T <#cols> -Y <str> -o <dir> -r <#cols> -s <prog> Aa All Also, pressing Esc twice and then typing a three-digit decimal number from 000 to 255 will enter the character with the corresponding value.  The following keystrokes are available in the main editor window.  Alternative keys are shown in parentheses:

 Append Append Selection to File Argument '%s' has an unterminated " At first message At last message Auto indent Auto save on exit, don't prompt Automatically indent new lines Back Background color "%s" cannot be bright Backspace Backup File Backup files Backwards Bad quote string %s: %s Bad regex "%s": %s Beg of Par Brought to you by: Browser Go To Directory Help Text

 Enter the name of the directory you would like to browse to.

 If tab completion has not been disabled, you can use the Tab key to (attempt to) automatically complete the directory name.

 The following function keys are available in Browser Go To Directory mode:

 Browser Search Command Help Text

 Enter the words or characters you would like to search for, and then press Enter.  If there is a match for the text you entered, the screen will be updated to the location of the nearest match for the search string.

 The previous search string will be shown in brackets after the search prompt.  Hitting Enter without entering any text will perform the previous search.

 Can now UnJustify! Can't go outside of %s in restricted mode Can't insert file from outside of %s Can't move up a directory Can't write outside of %s Cancel Cancel the current function Cancelled Cannot add a color command without a syntax command Cannot add a header regex without a syntax command Cannot add a linter without a syntax command Cannot add a magic string regex without a syntax command Cannot add formatter without a syntax command Cannot map name "%s" to a function Cannot map name "%s" to a menu Cannot prepend or append to a symlink with --nofollow set Cannot unset option "%s" Case Sens Close Close the current file buffer / Exit from nano Color "%s" not understood.
Valid colors are "green", "red", "blue",
"white", "yellow", "cyan", "magenta" and
"black", with the optional prefix "bright"
for foreground colors. Color syntax highlighting Command "%s" not allowed in included file Command "%s" not understood Command to execute [from %s]  Command to execute in new buffer [from %s]  Constant cursor position display Constantly show cursor position Conversion of typed tabs to spaces Convert typed tabs to spaces Copy Text Copy the current line and store it in the cutbuffer Could not create pipe Could not find syntax "%s" to extend Could not fork Could not get size of pipe buffer Couldn't determine hostname for lock file: %s Couldn't determine my identity for lock file (getpwuid() failed) Couldn't reopen stdin from keyboard, sorry
 Count the number of words, lines, and characters Creating misspelled word list, please wait... Cur Pos Cut Text Cut from cursor to end of line Cut from the cursor position to the end of the file Cut the current line and store it in the cutbuffer Cut to end CutTillEnd DIR: DOS Format Delete Delete the character to the left of the cursor Delete the character under the cursor Detect word boundaries more accurately Detected a legacy nano history file (%s) which I moved
to the preferred location (%s)
(see the nano FAQ about this change) Detected a legacy nano history file (%s) which I tried to move
to the preferred location (%s) but encountered an error: %s Directory for saving unique backup files Display the position of the cursor Display this help text Do not read the file (only write it) Do quick statusbar blanking Don't add newlines to the ends of files Don't convert files from DOS/Mac format Don't follow symbolic links, overwrite Don't hard-wrap long lines Don't look at nanorc files Don't show the two help lines Edit a replacement Enable alternate speller Enable smart home key Enable soft line wrapping Enable suspension Enable the use of the mouse End End of Par Enter Enter line number, column number Error deleting lock file %s: %s Error expanding %s: %s Error in %s on line %lu:  Error invoking "%s" Error invoking "sort -f" Error invoking "spell" Error invoking "uniq" Error opening lock file %s: %s Error reading %s: %s Error reading lock file %s: Not enough data read Error writing %s: %s Error writing backup file %s: %s Error writing lock file %s: %s Error writing lock file: Directory '%s' doesn't exist Error writing temp file: %s Execute Command Execute Command Help Text

 This mode allows you to insert the output of a command run by the shell into the current buffer (or a new buffer in multiple file buffer mode).  If you need another blank buffer, do not enter any command.

 The following function keys are available in Execute Command mode:

 Execute external command Exit Exit from nano Exit from the file browser Failed to write backup file, continue saving? (Say N if unsure)  Fatal error: no keys mapped for function "%s".  Exiting.
 File %s is being edited (by %s with %s, PID %d); continue? File Browser Help Text

 The file browser is used to visually browse the directory structure to select a file for reading or writing.  You may use the arrow keys or Page Up/Down to browse through the files, and S or Enter to choose the selected file or enter the selected directory.  To move up one level, select the directory called ".." at the top of the file list.

 The following function keys are available in the file browser:

 File Name to Append to File Name to Prepend to File Name to Write File exists, OVERWRITE ?  File to insert [from %s]  File to insert into new buffer [from %s]  File was modified since you opened it, continue saving ?  File: Finished Finished checking spelling Finished formatting First File First Line Fix Backspace/Delete confusion problem Fix numeric keypad key confusion problem For ncurses: Formatter Forward FullJstify Function '%s' does not exist in menu '%s' Get Help Go To Dir Go To Directory Go To Line Go To Line Help Text

 Enter the line number that you wish to go to and hit Enter.  If there are fewer lines of text than the number you entered, you will be brought to the last line of the file.

 The following function keys are available in Go To Line mode:

 Go To Text Go back one character Go back one word Go forward one character Go forward one word Go just beyond end of paragraph; then of next paragraph Go one screenful down Go one screenful up Go to beginning of current line Go to beginning of paragraph; then of previous paragraph Go to directory Go to end of current line Go to file browser Go to line and column number Go to next line Go to next linter msg Go to previous line Go to previous linter msg Go to the first file in the list Go to the first line of the file Go to the last file in the list Go to the last line of the file Go to the matching bracket Go to the next file in the list Go to the previous file in the list Got 0 parsable lines from command: %s Hard wrapping of overlong lines Help mode Home I can't find my home directory!  Wah! If needed, use nano with the -I option to adjust your nanorc settings.
 If you have selected text with the mark and then search to replace, only matches in the selected text will be replaced.

 The following function keys are available in Search mode:

 If you need another blank buffer, do not enter any filename, or type in a nonexistent filename at the prompt and press Enter.

 The following function keys are available in Insert File mode:

 In Selection:   Indent Text Indent the current line Insert File Help Text

 Type in the name of a file to be inserted into the current file buffer at the current cursor location.

 If you have compiled nano with multiple file buffer support, and enable multiple file buffers with the -F or --multibuffer command line flags, the Meta-F toggle, or a nanorc file, inserting a file will cause it to be loaded into a separate buffer (use Meta-< and > to switch between file buffers).   Insert a newline at the cursor position Insert a tab at the cursor position Insert another file into the current one Insert the next keystroke verbatim Internal error: can't match line %d.  Please save your work. Internal error: cannot set up redo.  Please save your work. Internal error: unknown type.  Please save your work. Invalid line or column number Invoke formatter, if available Invoke the linter, if available Invoke the spell checker, if available Invoking formatter, please wait Invoking linter, please wait Invoking spell checker, please wait Justify Justify the current paragraph Justify the entire file Key invalid in non-multibuffer mode Key name is too short Key name must begin with "^", "M", or "F" Last File Last Line Log & read location of cursor position Log & read search/replace string history Mac Format Main nano help text

 The nano editor is designed to emulate the functionality and ease-of-use of the UW Pico text editor.  There are four main sections of the editor.  The top line shows the program version, the current filename being edited, and whether or not the file has been modified.  Next is the main editor window showing the file being edited.  The status line is the third line from the bottom and shows important messages.   Mark Set Mark Text Mark Unset Mark text starting from the cursor position Missing color name Missing formatter command Missing key name Missing linter command Missing magic string name Missing option Missing regex string Missing syntax name Modified Mouse support Must specify a function to bind the key to Must specify a menu (or "all") in which to bind/unbind the key New Buffer New File Next File Next Line Next Lint Msg Next Page Next Word NextHstory Nn No No Replace No conversion from DOS/Mac format No current search pattern No file name No linter defined for this type of file! No matching bracket No more open file buffers Non-blank characters required Not a bracket Nothing in undo buffer! Nothing to re-do! Option		GNU long option		Meaning
 Option		Meaning
 Option "%s" requires an argument Option is not a valid multibyte string Path %s is not a directory and needs to be.
Nano will be unable to load or save search history or cursor positions.
 Prepend Prepend Selection to File Preserve XON (^Q) and XOFF (^S) keys Prev File Prev Line Prev Lint Msg Prev Page Prev Word PrevHstory Print version information and exit Quoting string Read %lu line Read %lu lines Read %lu line (Converted from DOS and Mac format - Warning: No write permission) Read %lu lines (Converted from DOS and Mac format - Warning: No write permission) Read %lu line (Converted from DOS and Mac format) Read %lu lines (Converted from DOS and Mac format) Read %lu line (Converted from DOS format - Warning: No write permission) Read %lu lines (Converted from DOS format - Warning: No write permission) Read %lu line (Converted from DOS format) Read %lu lines (Converted from DOS format) Read %lu line (Converted from Mac format - Warning: No write permission) Read %lu lines (Converted from Mac format - Warning: No write permission) Read %lu line (Converted from Mac format) Read %lu lines (Converted from Mac format) Read File Reading File Reading from stdin, ^C to abort
 Recall the next search/replace string Recall the previous search/replace string Received SIGHUP or SIGTERM
 Redid action (%s) Redo Redo the last undone operation Refresh Refresh (redraw) the current screen Regex strings must begin and end with a " character Regexp Repeat the last search Replace Replace a string or a regular expression Replace this instance? Replace with Replaced %lu occurrence Replaced %lu occurrences Requested fill size "%s" is invalid Requested tab size "%s" is invalid Restricted mode Reverse the direction of the search Save backups of existing files Save file under DIFFERENT NAME ?  Save modified buffer (ANSWERING "No" WILL DESTROY CHANGES) ?  Save modified buffer before linting? Scroll Down Scroll Up Scroll by line instead of half-screen Scroll down one line without scrolling the cursor Scroll up one line without scrolling the cursor Search Search Command Help Text

 Enter the words or characters you would like to search for, and then press Enter.  If there is a match for the text you entered, the screen will be updated to the location of the nearest match for the search string.

 The previous search string will be shown in brackets after the search prompt.  Hitting Enter without entering any text will perform the previous search.   Search Wrapped Search for a string Search for a string or a regular expression Set hard-wrapping point at column #cols Set operating directory Set width of a tab to #cols columns Silently ignore startup issues like rc file errors Smart home key Smooth scrolling Soft wrapping of overlong lines Sorry, keystroke "%s" may not be rebound Special thanks to: Spell Check Help Text

 The spell checker checks the spelling of all text in the current file.  When an unknown word is encountered, it is highlighted and a replacement can be edited.  It will then prompt to replace every instance of the given misspelled word in the current file, or, if you have selected text with the mark, in the selected text.

 The following function keys are available in Spell Check mode:

 Spell checking failed: %s Spell checking failed: %s: %s Start at line LINE, column COLUMN Suspend Suspension Switch to the next file buffer Switch to the previous file buffer Switched to %s Syntax "%s" has no color commands Syntax definition to use for coloring Tab Thank you for using nano! The "default" syntax must take no extensions The "none" syntax is reserved The Free Software Foundation The bottom two lines show the most commonly used shortcuts in the editor.

 The notation for shortcuts is as follows: Control-key sequences are notated with a caret (^) symbol and can be entered either by using the Control (Ctrl) key or pressing the Escape (Esc) key twice.  Escape-key sequences are notated with the Meta (M-) symbol and can be entered using either the Esc, Alt, or Meta key depending on your keyboard setup.   The nano text editor This is the only occurrence This message is for unopened file %s, open it in a new buffer? To Bracket To Files To Linter To Spell Toggle appending Toggle backing up of the original file Toggle prepending Toggle the case sensitivity of the search Toggle the use of DOS format Toggle the use of Mac format Toggle the use of a new buffer Toggle the use of regular expressions Too many backup files? Two single-column characters required Type '%s -h' for a list of available options.
 Unable to create directory %s: %s
It is required for saving/loading search history or cursor positions.
 Uncut Text Uncut from the cutbuffer into the current line Undid action (%s) Undo Undo the last operation Unicode Input Unindent Text Unindent the current line Unjustify Unknown Command Unknown option "%s" Usage: nano [OPTIONS] [[+LINE,COLUMN] FILE]...

 Use "fg" to return to nano.
 Use (vim-style) lock files Use bold instead of reverse video text Use of one more line for editing Use one more line for editing Verbatim Verbatim Input View View mode (read-only) Warning: Modifying a file which is not locked, check directory permission? Where Is WhereIs Next Whitespace display Window size is too small for nano...
 Word Count Write File Help Text

 Type the name that you wish to save the current file as and press Enter to save the file.

 If you have selected text with the mark, you will be prompted to save only the selected portion to a separate file.  To reduce the chance of overwriting the current file with just a portion of it, the current filename is not the default in this mode.

 The following function keys are available in Write File mode:

 Write Out Write Selection to File Write the current file to disk Wrote %lu line Wrote %lu lines XOFF ignored, mumble mumble XON ignored, mumble mumble Yes Yy and anyone else we forgot... disabled enable/disable enabled line %ld/%ld (%d%%), col %lu/%lu (%d%%), char %lu/%lu (%d%%) line break line join nano is out of memory! text add text cut text delete text insert text replace text uncut the many translators and the TP version Project-Id-Version: nano-2.3.99pre4
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-06 00:22-0500
PO-Revision-Date: 2015-04-13 19:17+0200
Last-Translator: Мирослав Николић <miroslavnikolic@rocketmail.com>
Language-Team: Serbian <(nothing)>
Language: sr
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 
 Уграђене опције: 
Међумеморија није уписана у %s:%s
 
Међумеморија није уписана: %s
 
Међумеморија је уписана у %s
 
Притисните Унеси да наставите
 
Притисните Унеси да наставите покретање наноа
  (за замену)  (за замену) у избору  Ел. пошта: nano@nano-editor.org	Веб: http://www.nano-editor.org/  Гнуов нано, издање %s
  Следећи тастери су доступни у режиму претраге прегледача:

  [Резерва]  [уназад]  [разл. величину слова]  [ДОС запис]  [Мек запис]  [рег. израз] Нисам нашао „%.*s%s“ „%s“ је датотека уређаја „%s“ је директоријум Нисам нашао „%s“ „start=“ захтева и одговарајући „end=“ %sРечи: %lu  Редова: %ld  Знакова: %lu (дир) (још) (главни дир) +РЕД,СТУБАЦ --backupdir=[дир] --fill=[#стубац] --operatingdir=[дир] --quotestr=[ниска] --speller=[програм] --syntax=[ниска] --tabsize=[#ступци] -C [дир] -Q [ниска] -T [#ступци] -Y [ниска] -o [дир] -r [#стубац] -s [програм] АаАа Све Такође, двоструким притиском на тастер Ескејп и укуцавањем троцифреног децималног броја од 000 до 255 унећете знак са одговарајућом вредношћу. Следећи притисци тастера су доступни у главном прозору уређивача. Заменски тастери су приказани у загради:

 Додај позади Додаје избор на крај датотеке Аргумент „%s“ садржи недовршени знак " На прву поруку На последњу поруку Самоувлачење Сам чува при излазу, не пита Самостално увлачи нове редове Назад Боја позадине „%s“ не може бити светла Врати простор Направи резерву Резервне датотеке Уназад Лоша ниска за цитирање %s: %s Лош регуларни израз „%s“: %s Почeтaк пасуса Припремили: Помоћ за одлазак у директоријум

 Унесите назив директоријума у који желите да одете.

 Уколико није искључено допуњавање табулатором, можете користити TAB тастер да (покушате да) самодопуните назив директоријума.

 Следећи тастери су доступни у режиму одласка у директоријум:

 Помоћ за наредбу претраге прегледача

 Унесите речи или знаке које желите да нађете, и притисните Унеси. Уколико постоји тражени текст, на екрану ће се приказати положај најближег резултата претраге.

 Претходна ниска претраге ће се приказати у угластим заградама након „Тражи:“. Притиском на Унеси без уноса неког текста ће обавити претходну претрагу.

 Сада могу да „одравнам“! Не могу да идем ван „%s“ у ограниченом режиму Не могу да уметнем датотеку изван %s Не могу да идем директоријум изнад Не могу да пишем ван %s Откажи Отказује текућу операцију Отказано Не могу да додам наредбу боје без наредбе синтаксе Не могу да додам регуларни израз заглавља без наредбе синтаксе Не могу да додам чистача без наредбе синтаксе Не могу да додам регуларни израз чаробне ниске без наредбе синтаксе Не могу да додам обликовача без наредбе синтаксе Не могу да мапирам назив „%s“ ка функцији Не могу да мапирам назив „%s“ ка изборнику Не могу да додам подешавање „--nofollow“ испред или иза симболичке везе Не могу да расподесим опцију „%s“ Вел. слова Затвори Затвара међумеморију тренутне датотеке / излази из нана Нисам разумео боју „%s“.
Дозвољене боје су „green“ (зелена), „red“ (црвена), „blue“ (плава), 
„white“ (бела), „yellow“ (жута), „cyan“ (плавичаста), „magenta“ (љубичаста) и
„black“ (црна), са необавезним предметком „bright“ (светло)
за боје позадине. Истицање синтаксе у боји Наредба „%s“ није дозвољена у укљученој датотеци Нисам разумео наредбу „%s“ Наредба за извршавање [из „%s“]  Наредба за извршавање у новој међумеморији [из „%s“]  Сталан приказ положаја курзора Непрекидно приказује положај курзора Претварање укуцаних табулатора у размаке Претвара укуцане табулаторе у размаке Умножи текст Исеца текући ред и смешта га у исечке Не могу да направим спојку Не могу да нађем синтаксу „%s“ за ширење Не могу да расцепим Не могу да добавим величину међумеморије спојке Не могу да одредим назив домаћина за датотеку закључавања: %s Не могу да одредим мој идентет за датотеку закључавања (није успело „getpwuid()“) Не могу поново да отворим стандардни улаз са тастатуре, извините
 Броји речи, редова, и знакова Правим списак погрешно унетих речи, сачекајте... Положај курзора Исеци текст Исеца од курзора до краја реда Одсеца од положаја курзора до краја датотеке Исеца текући ред и смешта га у исечке Исецање до краја Исеци до краја ДИР:  ДОС запис Обриши Брише знак лево од курзора Брише знак под курзором Тачније открива границе речи Открио сам стару датотеку историјата наноа (%s) коју сам
преместио на жељено место (%s)
(погледајте нанова ЧПП о овој измени) Открио сам стару датотеку историјата наноа (%s) коју сам покушао да
преместим на жељено место (%s) али сам наишао на грешку: %s Директоријум за чување јединствених датотека резерве Приказује положај курзора Приказује текст ове помоћи Не чита датотеку (само је пише) Обавља брзо чишћење траке стања Не додаје нове редове на крајеве датотека Не претвара датотеке из ДОС/Мекинтош записа Не прати симболичке везе, преснимава их Не прелама јако дуге редове Не тражи у нанорц датотекама Не приказује два реда помоћи Измените замену Укључује заменску проверу правописа Укључује паметни тастер „Почетак“ Укључује меко преламање редова Укључује обуставу Укључује коришћење миша Крај Крај пасуса Унеси Унесите број реда, број ступца Грешка брисања датотеке закључавања „%s“: %s Грешка ширења „%s“: %s Грешка у %s у %lu. реду:  Грешка при покретању „%s“-а Грешка при покретању „sort -f“-а Грешка при покретању „spell“-а Грешка при покретању „uniq“-а Грешка отварања датотеке закључавања „%s“: %s Грешка при читању „%s“: %s Грешка читања датотеке закључавања „%s“: Нема довољно података читања Грешка приликом писања „%s“: %s Грешка записивања датотеке резерве „%s“: %s Грешка записивања датотеке закључавања „%s“: %s Грешка писања датотеке закључавања: Директоријум „%s“ не постоји Грешка писања привремене датотеке: %s Изврши наредбу Помоћ за извршавање наредбе

 Овај режим вам омогућава да уметнете излаз наредбе коју покреће љуска у текућу међумеморију (или у нову међумеморију у режиму међумеморије више датотека). Ако вам је потребна још једна празна међумеморија, немојте уносити никакву наредбу.

 Следећи тастери су доступни у овом режиму:

 Извршава спољну наредбу Изађи Излази из нана Излази из прегледача датотека Нисам успео да запишем датотеку резерве, да наставим са чувањем? (Одговорите са N ако нисте сигурни) Кобна грешка: нема мапираних тастера за функцију „%s“.  Излазим.
 Датотека „%s“ је измењена (од стране %s са %s, ЛИБ %d); да наставим? Помоћ за прегледач датотека

 Прегледач датотека се користи за визуелно разгледање директоријума за избор датотеке ради читања или уписа. Можете користити стрелице или тастере „Страница горе/доле“ за разгледање датотека, а S или Унеси да изаберете означену датотеку или да уђете у означени директоријум. Да одете један ниво изнад, изаберите директоријум са називом „..“ на врху списка датотека.

 Следећи тастери су доступни у режиму прегледача датотека:

 Назив датотеке за којом ће следити Назив датотеке којој ће претходити Назив датотеке у којој ће уписати Датотека постоји, да ПРЕСНИМИМ? Датотека за унос [из „%s“]  Датотека за унос у нову међумеморију [из „%s“]  Датотека је измењена од када сте је отворили, да наставим са чувањем ? Датотека: Завршено Завршио сам проверу правописа Обликовање је завршено Прва датотека Први ред Поправља проблеме тастера Поврати простор/Обриши Поправља проблеме тастера нумеричке тастатуре За ен-курсис: Обликовач Напред Пуно поравнање Функција „%s“ не постоји у изборнику „%s“ Добави помоћ У директорјум Иди у директоријум На ред Помоћ за одлазак у ред

 Унесите број реда у који желите да идете и притисните Унеси. Уколико има мање редова текста од броја који сте унели, поставићу вас на последњи ред датотеке.

 Следећи тастери су доступни у режиму одласка у ред:

 На текст Иде један знак уназад Иде једну реч уназад Иде један знак унапред Иде једну реч унапред Иде одмах после краја пасуса; затим следећег пасуса На претходни екран На следећи екран На почетак текућег реда На почетак пасуса; затим претходног пасуса Иде у директоријум На крај текућег реда Иде у прегледач датотека На ред и број ступца На следећи ред Иде на следећу поруку чистача На претходни ред Иде на претходну поруку чистача Иде у прву датотеку на списку Иде у први ред датотеке Иде у последњу датотеку на списку Иде у последњи ред датотеке На одговарајућу заграду Иде у следећу датотеку на списку На претходну датотеку на списку Добих 0 обрадивих редова од наредбе: %s Јако преламање превише дугих редова Режим помоћи Почетак Ја не могу да нађем мој лични директоријум! Бре, брате! Ако је потребно, користите нано са опцијом „-I“ да дотерате ваша подешавања нанорц-а
 Ако сте изабрали текст са означавачем а затим претрагу да замените, само поклапања у изабраном тексту ће бити замењена.

 Следећи тастери су доступни у режиму претраге:

 Уколико вам треба још једна празна међумеморија, немојте унети назив датотеке, или укуцајте назив непостојеће датотеке и притисните Унеси.

 Следећи тастери су доступни у режиму уметања датотеке:

 У избору: Увуци текст Увлачи текући ред Помоћ за уметање датотеке

 Унесите назив датотеке коју желите да уметнете у текућу међумеморију на текућем положају курзора.

 Уколико сте наноа изградили са подршком међумеморије за више датотека, и укључили међумеморије више датотека са опцијама „-F“ или „--multibuffer“, Мета-F изменом, или нанорц датотеком, уметање датотеке ће је учитати у одвојеној међумеморији (користите Мета-< и > за пребацивање између међумеморија). Умеће нови ред на положај курсора Умеће табулатор на положај курсора Умеће другу датотеку у текућу Умеће следећу дословност притиска тастера Унутрашња грешка: не могу да поклопим %d. ред.  Молим сачувајте ваш рад. Унутрашња грешка: не могу да поставим повраћај.  Молим сачувајте ваш рад. Унутрашња грешка: непозната врста.  Молим сачувајте ваш рад. Неисправан број реда или ступца Покреће обликовача, ако је доступан Покреће чистача, ако је доступан Покреће проверу правописа, ако је доступна Призивам обликовача, сачекајте мало Призивам чистача, сачекајте мало Призивам проверу правописа, сачекајте мало Пораванај Поравнава текући пасус Поравнава читаву датотеку Недозвољен тастер у не-вишемеђумеморијском режиму Назив тастера је прекратак Назив тастера мора почети са ^, M, или F Пследња датотека Последњи ред Бележи и чита место положаја курзора Бележи и чита историјат претраге/замене ниске Мекинтош запис  Главна нанова помоћ

 Уређивач нано је израђен да опонаша могућности и лакоћу употребе уређивача Пико са Универзитета у Вашингтону. Постоје четири главна одељка уређивача: горњи ред приказује издање програма, назив датотеке која се управо уређује, и да ли је датотека измењена или не. Следећи део је главни прозор уређивача који приказује датотеку која се уређује. Ред са стањем је трећи ред одоздо и приказује важне поруке. Постављена ознака Означи текст Поништена ознака Означава текст почевши од положаја курзора Недостаје назив боје Недостаје наредба обликовача Недостаје назив тастера Недостаје наредба чистача Недостаје назив чаробне ниске Недостаје опција Недостаје ниска регуларног израза Недостаје назив синтаксе Измењено  Подршка за миша Морате навести функцију за коју свезати тастер Морате навести изборник (или „all“) у коме свезати/развезати тастер Нова међумеморија Нова датотека Следећа датотека Следећи ред След пор чистч Следећа страна Следећа реч След. историјат НнNn Не Не мењај Без претварања из Мекинтош/ДОС записа Нема шаблона текуће претраге Нема назива датотеке Чистач није одређен за ову врсту датотеке! Нема одговарајуће заграде Нема више међумеморије отворених датотека Потребни су не-празни знаци Није заграда Нема ничега у међумеморији поништавања! Ничега за поновно обављање! Опција		Гнуова дуга опција	Значење
 Опција		Значење
 Опција „%s“ захтева аргумент Опција није исправна вишебитна ниска Путања „%s“ није директоријум а мора бити.
Нано неће бити у могућности да учита или да сачува историјат претраге или положаје курсора.
 Додај напред Додаје избор на почетак датотеке Очувава тастере XON (^Q) и XOFF (^S) Претходна датотека Претходни ред Пртх пор чистч Претходна страна Претходна реч Прет. историјат Исписује податке о издању и излази Ниска за цитирање Учитах %lu ред Учитах %lu реда Учитах %lu редова Учитах %lu ред (претворен из ДОС и Мекинтош записа — упозорење: нема дозволе за писање) Учитах %lu реда (претворених из ДОС и Мекинтош записа — упозорење: нема дозволе за писање) Учитах %lu редова (претворених из ДОС и Мекинтош записа — упозорење: нема дозволе за писање) Учитах %lu ред (претворен из ДОС и Мекинтош записа) Учитах %lu реда (претворених из ДОС и Мекинтош записа) Учитах %lu редова (претворених из ДОС и Мекинтош записа) Учитах %lu ред (претворен из ДОС записа — упозорење: нема дозвола за писање) Учитах %lu реда (претворених из ДОС записа — упозорење: нема дозвола за писање) Учитах %lu редова (претворених из ДОС записа — упозорење: нема дозвола за писање) Учитах %lu ред (претворен из ДОС записа) Учитах %lu реда (претворених из ДОС записа) Учитах %lu редова (претворених из ДОС записа) Учитах %lu ред (претворен из Мекинтош записа — упозорење: нема дозвола за писање) Учитах %lu реда (претворених из Мекинтош записа — упозорење: нема дозвола за писање) Учитах %lu редова (претворених из Мекинтош записа — упозорење: нема дозвола за писање) Учитах %lu ред (претворен из Мекинтош записа) Учитах %lu реда (претворених из Мекинтош записа) Учитах %lu редова (претворених из Мекинтош записа) Читај датотеку Учитавам датотеку Читам са стандардног улаза, ^C да прекинете
 Опозива ниску следеће претраге и замене Опозива ниску претходне претраге и замене Примих SIGHUP или SIGTERM
 Радња поновног обављања (%s) Поврати Враћа последњу поништену радњу Освежи Освежава (поново исцртава) текући екран Ниске регуларних израза морају почети и завршити се знаком " Рег. израз Понавља последњу претрагу Замени Замењује ниску или регуларни израз Да заменим ову појаву? Замени са Замених %lu појаву Замених %lu појаве Замених %lu појава Тражена величина попуне %s је неисправна Тражена величина табулатора %s је неисправна Ограничени режим Заокреће правац претраге Чува резерве постојећих датотека Да сачувам датотеку под ДРУГАЧИЈИМ НАЗИВОМ ? Да сачувам измењену међумеморију (ОДГОВОР „Не“ ЋЕ ОДБАЦИТИ ИЗМЕНЕ) ?  Да сачувам измењену међумеморију пре чишћења? Помакни доле Помакни горе Премиче по ред уместо пола екрана Помера се један ред доле без померања курсора Помера се један ред горе без померања курсора Тражи Помоћ за наредбу претраге

 Унесите речи или знаке које желите да нађете, и притисните Унеси. Уколико постоји тражени текст, на екрану ће се приказати положај најближег резултата претраге.

 Претходна ниска претраге ће се приказати у угластим заградама након „Тражи:“. Притиском на Унеси без уноса неког текста ће обавити претходну претрагу. Тражи у круг Тражи ниску Тражи ниску или регуларни израз Поставља тачку јаког прелома на ступцу бр. стубац Поставља радни директоријум Поставља ширину табулатора на „#ступци“ стубаца Нечујно занемарује проблеме покретања као што су грешке рц датотеке Паметни тастер „Почетак“ Глатко клизање Слабо преламање превише дугих редова Извините, ниска тастера „%s“ не може бити поново свезана Посебно се захваљујемо: Помоћ за проверу правописа

 Провера правописа ради на свом тексту текуће датотеке.  Када се наиђе на непознату реч, она се истиче и замена се може уредити.  Тада ћете бити упитани да замените сваку појаву дате погрешно унете речи у текућој датотеци.

 Следећи тастери су доступни у режиму провере правописа:

 Провера правописа није успела: %s Провера правописа није успела: %s: %s Почиње у реду РЕД, ступцу СТУБАЦ Обустави Обустави Пребацује се на међумеморију следеће датотеке Пребацује се на међумеморију претходне датотеке Прешао сам на %s Синтакса „%s“ нема наредбе боје Одредница синтаксе за обојавање Табулатор Хвала вам што користите нано! Синтакса „default“ не сме да садржи проширења Синтакса „none“ је резервисана Задужбини слободног софтвера  Два доња реда приказују најчешће коришћене пречице у уређивачу.

 Запис пречица је овакав: Пречице тастера Контрол су означене помоћу симбола капице (^) и уносе се уз тастер Контрол (Ctrl) или двоструким притиском на тастер Ескејп (Esc).  Пречице тастера Ескејп су означене Мета (М) симболом и уносе се помоћу тастера „Esc“, „Alt“ или „Meta“ у зависности од подешавања ваше тастатуре. Уређивач текста нано Ово је једина појава Ова порука је за неотворену датотеку „%s“, да је отворим у новој међумеморији? До заграде У датотеке На чишћење На проверу писања Пребацује додавање након Пребацује стварање резерве изворне датотеке Пребацује додавање пре Пребацује осетљивост величине слова у претрази Пребацује коришћење ДОС записа Пребацује коришћење Мекинтош записа Пребацује коришћење нове међумеморије Пребацује коришћење регуларних израза Превише резервних датотека? Потребна су два знака једног-ступца Укуцајте „%s -h“ за списак доступних опција.
 Не могу да направим директоријум „%s“: %s
Потребан је за чување/учитавање историјата претраге или положаја курсора.
 Не сеци текст Убацује исечак у текући ред Радња поништавања обављеног (%s) Опозови Поништава последњу радњу Улаз Уникода Поништи увлачење Поништава увлачење текућег реда Одравнај Непозната наредба Непозната опција „%s“ Употреба: nano [МОГУЋНОСТИ] [[+РЕД,СТУБАЦ] ДАТОТЕКА]...

 Користите „fg“ да се вратите у нано.
 Користи (вим-стила) датотеке закључавања Користи подебљање уместо обрнутог видео текста Користи још један ред за уређивање Користи још један ред за уређивање Вербатим Улаз дословности Преглед  Режим прегледа (само за читање) Упозорење: Мењам датотеку која није закључана, да проверим дозволе директоријума? Где је Где је слдћи Приказ празнина Величина прозора је премала за нана...
 Изброј речи Помоћ за упис датотеке

 Унесите назив датотеке у којој желите да сачувате текућу датотеку и притисните Унеси да сачувате датотеку.

 Уколико сте изабрали текст помоћу означавача, бићете упитани да ли желите да сачувате само изабрани део у засебну датотеку. Да умањите шансе преснимавања постојеће датотеке само једним њеним делом, назив текуће датотеке се не подразумева у овом режиму.

 Следећи тастери су доступни у режиму уписа датотеке:

 Испиши Уписује избор у датотеку Уписује текућу датотеку на диск Уписах %lu ред Уписах %lu реда Уписах %lu редова XOFF је занемарен, јао мени XON је занемарен, јао мени Да ДдDdYy и свима осталима које смо пропустили... искључено — укључује/искључује укључено ред %ld/%ld (%d%%), стубац %lu/%lu (%d%%), знак %lu/%lu (%d%%) крај реда спој реда наноу је понестало меморије! додавање текста исецање текста брисање текста уметање текста замена текста поништавање исецања текста многим преводиоцима и ТП-у издање  