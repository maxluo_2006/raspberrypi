��    �     <  _  \      �%     �%     �%     &     %&     <&  (   V&     &     �&  =   �&     �&  D   �&  	   B'     L'     Y'     k'     y'  	   �'     �'     �'     �'     �'  (   �'  $   (     +(     1(     8(     ?(     L(     Y(     k(     z(     �(     �(     �(     �(     �(     �(  
   �(     �(     �(  
   )  	   )     )     )  �   )     *     "*  #   ;*     _*     p*     �*     �*     �*     �*  &   �*  	   �*     +     +  	   +     $+     <+  
   O+     Z+  -  m+  �  �,     3.  )   F.  $   p.     �.     �.     �.     �.  	   �.  3   �.  2   */  ,   ]/  8   �/  -   �/  "   �/     0  9   30     m0  	   �0     �0  .   �0  �   �0     t1  )   �1     �1     �1  +   �1      2     ?2  "   _2     �2  	   �2  3   �2     �2  $   �2     3  !   '3  -   I3  @   w3  +   �3  0   �3  -   4     C4     K4     T4  3   s4  2   �4  
   �4  
   �4     �4  
   �4      5  .   5  %   65  &   \5  z   �5  z   �5  (   y6  "   �6     �6  $   �6     7  '   7  '   E7  &   m7     �7     �7     �7     �7     �7     8     *8     D8     V8     r8  
   v8     �8      �8     �8     �8     �8     �8     9     &9     =9     S9     r9  0   �9     �9      �9     �9  5   :     C:     _:  /  o:     �;     �;     �;     �;  @   �;  9   (<  :   b<  �  �<     P>     g>     >     �>     �>  )   �>  9   �>     *?     0?     9?     T?  
   h?  
   s?  &   ~?  (   �?     �?  	   �?     �?  
   �?  )   �?     "@  	   +@     5@  
   E@    P@  
   VA     aA     wA     �A     �A  7   �A     �A     B     B  8   7B     pB     �B     �B     �B     �B     �B     �B     C      C      ?C     `C     �C     �C     �C  #   �C  %   �C     %D  	   ED     OD  %   TD  G   zD  �   �D  �   xE     9F     IF     UF  �  mF  '   H  #   BH  (   fH  "   �H  <   �H  ;   �H  5   +I     aI     I     �I  &   �I     �I     J  #   "J     FJ     NJ     lJ  #   �J     �J  )   �J  	   �J  	   �J  &   �J  (   #K  
   LK  �  WK     M  	   M  
   M  +   *M     VM     iM     �M     �M     �M     �M     �M     �M     �M     N  *   N  >   ?N  
   ~N     �N  	   �N  	   �N     �N  	   �N  	   �N  
   �N     �N     �N  
   �N  !   �N     O      O  (   -O     VO     jO     �O     �O     �O     �O  !   �O     �O      P  &   .P  t   UP     �P     �P  $   �P  	   Q  	   Q     %Q  	   3Q  	   =Q  
   GQ  "   RQ     uQ     �Q  �   �Q  d   DR  �   �R  T   <S  �   �S  T   $T  	   yT  (   �T     �T  !   �T      �T  %   �T  )   "U     LU     hU     zU     U     �U  #   �U  3   �U     �U     V     V  (   $V     MV     dV  0   qV  #   �V  "   �V     �V  #   �V     W  !   <W  =   ^W  $   �W     �W  	   �W  %   �W  1   �W  /   /X     _X  �  fX     �Y     Z  +   Z  '   EZ     mZ  #   �Z  2   �Z     �Z     �Z     �Z  (   [     E[  �  X[     �\     ]  !   /]     Q]  
   Y]     d]  "   �]     �]  !   �]  %   �]     �]     ^  ,   ^     H^     f^  �  �^     /`     D`  >   ``  
   �`     �`  	   �`     �`     �`  &   �`     �`  )   a     :a     Wa     ta  %   �a     �a  %   �a  .   �a  h   %b  
   �b  .   �b     �b     �b     �b     �b     c     c  	   -c     7c     Gc  0   [c     �c     �c  &   �c      �c     d     *d     3d     Bd     Gd  J   ]d     �d     �d     �d  %   �d  
   �d  �  e  	   �f     �f     �f     �f     g     .g     Ig     Mg     Pg     mg     vg     �g  <   �g  
   �g  	   �g     �g     �g     �g     h     h      h  
   -h     8h     Xh  �  `h     �i  %   	j      /j     Pj     gj  ,   �j     �j      �j  =   �j      k  H   8k     �k     �k     �k     �k     �k  	   �k     �k  #   �k     l     /l  /   Fl  -   vl     �l     �l     �l     �l     �l     �l     �l     �l     m      m     5m     Gm     [m     dm     pm     }m     �m     �m     �m     �m     �m  #  �m     �n     �n  ,   o     /o     Io     do  0   ro  '   �o     �o  /   �o     p     	p     p  	   +p  ,   5p     bp     ~p     �p  a  �p  �  �q     �s  +   �s  1   �s  "   %t      Ht     it     rt  	   �t  D   �t  S   �t  :   1u  R   lu  =   �u  %   �u  "   #v  a   Fv  *   �v     �v     �v  *   �v  �   w     �w  3   �w     'x     Bx  1   `x  ,   �x  .   �x  2   �x  -   !y  
   Oy  3   Zy  (   �y  5   �y     �y  B   z  I   Nz  Q   �z  *   �z  3   {  A   I{  	   �{  
   �{  )   �{  /   �{  3   �{  0   .|     _|     k|  
   p|     {|  -   �|  "   �|  2   �|  �   }  �   �}  -   *~     X~     x~  %   �~  %   �~  5   �~  2     /   @  &   p     �  #   �     �  /   �  )   !�      K�     l�     ��     ��     ��     ��  0   ��  2   �     �     6�     U�     m�     ��     ��  /   ��     �  N   �     W�  3   t�  2   ��  G   ۂ  ,   #�  
   P�  P  [�     ��     Ǆ     ̈́     ۄ  d   ��  I   `�  M   ��    ��  !    �  #   "�     F�  $   d�      ��  3   ��  K   ވ     *�     3�  #   ;�     _�  
   |�     ��  5   ��  8   ɉ     �  
   �     �  
   #�  /   .�  	   ^�     h�     q�     ��  &  ��  
   ��     ��     ݋     ��     �  =   4�     r�     ��  #   ��  @   ό     �     #�     C�     _�     y�  %   ��     ��  $   Ѝ      ��  "   �  !   9�  "   [�     ~�  #   ��  "   ��  ;   �  !   �     A�     O�  1   V�  S   ��  �   ܏  �   ��     ��     ��     ��  �  ʑ  7   z�  ?   ��  "   �  ,   �  F   B�  E   ��  4   ϔ  &   �  +   +�  6   W�  7   ��  1   ƕ  9   ��  :   2�  
   m�     x�     ��  '   ��  /   ݖ  D   �     R�     ^�  +   k�  =   ��  
   ՗  �  ��     ��  
   Й     ۙ  1   �     �  4   4�  "   i�  1   ��  $   ��     �     ��      �  
   ,�     7�  9   K�  N   ��     ԛ     �     �     ��  
   �  
   �  
   �  
   %�     0�     3�  
   6�  %   A�     g�     ��  D   ��      ޜ  )   ��  *   )�     T�     d�     ��  $   ��     ��  &   ѝ  -   ��  �   &�  	   ��     ��  %   ݞ     �     �  
   �  
   #�  
   .�  
   9�  .   D�     s�  %   ��  �   ��  r   e�  �   ؠ  d   ��  �   �  d   ��  
   ��  N   �     V�  .   f�  "   ��  4   ��  1   ��     �     :�     O�  &   W�  
   ~�     ��  ?   ��  	   �     �  
   �  *   �     C�     `�  7   o�  *   ��  ,   ҥ     ��  "   �  +   3�  .   _�  H   ��  =   צ  
   �  
    �  6   +�  7   b�  8   ��     ӧ  �  ڧ     ��     ��  &   ��  0   ԩ  &   �  *   ,�  .   W�     ��     ��     ��  /   ֪      �  �  '�  (    �  ,   I�     v�  	   ��     ��  &   ��  %   ϭ     ��  -   	�  -   7�     e�     i�  +   ��  $   ��     Ѯ    ��     �     �  L   -�     z�  
   ��  	   ��     ��     ��  -   ��     �  6   �     =�     \�     {�  '   ��  "   ò  .   �  ,   �  �   B�  	   ǳ  &   ѳ     ��     �     �     6�     I�  "   U�  
   x�     ��     ��  @   ��     �  %   �  &   7�  "   ^�  "   ��     ��     ��     ��  &   ��  J   �     2�  	   9�     C�  .   W�  
   ��  �  ��     k�     s�  "   ��  '   ��     ݸ     ��     �     �  -   �     C�     Q�  
   d�  =   o�     ��     ��     Ϲ     �      �     �     %�     9�     R�     b�     z�         �      f  �  �  =  v   8   _  �       �  �                   {   B  �   �  �   C      �      �  ~       '                     �       K   S     (       �   �   �   �       J      �  A       �   n         ^       �      �  k  )  �   �  v  �   <  �   g               X  :  �          �   �   e   b  t   ;       �   �       V  $            �   y   ?   >              3   @  �   �  �   {  �      =   �     r     >   �   �   ?  �   �       E   �  �  D   &      �                       W       �  �  �      Q  �   .   a     
      "  .      �   �      P       �       X       +   �  R  �   �   %       z      �       �   a  5   �   �  �   �      �      �   p     [      �     �      �   �           �   �   J  �  �   j   �  s       �              �       �   �  H   �   �   �  &   I  D      �            �   �  7      P                        �  �           �          �  q   �   �   p   *   !       �          O  W      Q   �   K      �   `     �      �   -           m   �       0   @   �  �   �   "       �   6  <   9   �           /   `       �       �     b     i  5      �       �      �       \   w   �  �       U      �   �  M  �   T       �   ;          t  �  �           h  �       �   �        �      �  �   y  2  +  �  �        N       u     |  q      �      �          �   �   c   O       G  [               c  k   |   9      �   w  �  -  �       �  \  '    �      �  	  �      �      �       }            U       �   o             N  H  I   Y  ]   h           �   V   �       �      C   4     G   �   F   �    �       �       �  �             E      ,  �   e      �    �       �   7   0  �      8  �   �  F  ]  �   l      /  �   j      �      �  �   �   %  3  �       x  u       	   1  !    �       �      }   �             �      �  x       �   (  �           _   �  Z  o  �   L  �  z      �    �      �          m  �           �         �   2   Z   �      B   Y   �  �   �   ,   �   g    �           f      *  #   r  �   �       �       d       s   �          ~  �          L   R   M   �   �   �  �   S   6   )   �   ^  i       �   l       �      n      �   �   
   �   �   1     #              �  d  T        �           �    �   :   A  �       4    $   
 Compiled options: 
Buffer not written to %s: %s
 
Buffer not written: %s
 
Buffer written to %s
 
Press Enter to continue
 
Press Enter to continue starting nano.
  (to replace)  (to replace) in selection  Email: nano@nano-editor.org	Web: http://www.nano-editor.org/  GNU nano, version %s
  The following function keys are available in Browser Search mode:

  [Backup]  [Backwards]  [Case Sensitive]  [DOS Format]  [Mac Format]  [Regexp] "%.*s%s" not found "%s" is a device file "%s" is a directory "%s" not found "start=" requires a corresponding "end=" %sWords: %lu  Lines: %ld  Chars: %lu (dir) (huge) (more) (parent dir) +LINE,COLUMN --backupdir=<dir> --fill=<#cols> --operatingdir=<dir> --quotestr=<str> --speller=<prog> --syntax=<str> --tabsize=<#cols> -C <dir> -Q <str> -T <#cols> -Y <str> -o <dir> -r <#cols> -s <prog> Aa All Also, pressing Esc twice and then typing a three-digit decimal number from 000 to 255 will enter the character with the corresponding value.  The following keystrokes are available in the main editor window.  Alternative keys are shown in parentheses:

 Append Append Selection to File Argument '%s' has an unterminated " At first message At last message Auto indent Auto save on exit, don't prompt Automatically indent new lines Back Background color "%s" cannot be bright Backspace Backup File Backup files Backwards Bad quote string %s: %s Bad regex "%s": %s Beg of Par Brought to you by: Browser Go To Directory Help Text

 Enter the name of the directory you would like to browse to.

 If tab completion has not been disabled, you can use the Tab key to (attempt to) automatically complete the directory name.

 The following function keys are available in Browser Go To Directory mode:

 Browser Search Command Help Text

 Enter the words or characters you would like to search for, and then press Enter.  If there is a match for the text you entered, the screen will be updated to the location of the nearest match for the search string.

 The previous search string will be shown in brackets after the search prompt.  Hitting Enter without entering any text will perform the previous search.

 Can now UnJustify! Can't go outside of %s in restricted mode Can't insert file from outside of %s Can't move up a directory Can't write outside of %s Cancel Cancel the current function Cancelled Cannot add a color command without a syntax command Cannot add a header regex without a syntax command Cannot add a linter without a syntax command Cannot add a magic string regex without a syntax command Cannot add formatter without a syntax command Cannot map name "%s" to a function Cannot map name "%s" to a menu Cannot prepend or append to a symlink with --nofollow set Cannot unset option "%s" Case Sens Close Close the current file buffer / Exit from nano Color "%s" not understood.
Valid colors are "green", "red", "blue",
"white", "yellow", "cyan", "magenta" and
"black", with the optional prefix "bright"
for foreground colors. Color syntax highlighting Command "%s" not allowed in included file Command "%s" not understood Command to execute [from %s]  Command to execute in new buffer [from %s]  Constant cursor position display Constantly show cursor position Conversion of typed tabs to spaces Convert typed tabs to spaces Copy Text Copy the current line and store it in the cutbuffer Could not create pipe Could not find syntax "%s" to extend Could not fork Could not get size of pipe buffer Couldn't determine hostname for lock file: %s Couldn't determine my identity for lock file (getpwuid() failed) Couldn't reopen stdin from keyboard, sorry
 Count the number of words, lines, and characters Creating misspelled word list, please wait... Cur Pos Cut Text Cut from cursor to end of line Cut from the cursor position to the end of the file Cut the current line and store it in the cutbuffer Cut to end CutTillEnd DIR: DOS Format Delete Delete the character to the left of the cursor Delete the character under the cursor Detect word boundaries more accurately Detected a legacy nano history file (%s) which I moved
to the preferred location (%s)
(see the nano FAQ about this change) Detected a legacy nano history file (%s) which I tried to move
to the preferred location (%s) but encountered an error: %s Directory for saving unique backup files Display the position of the cursor Display this help text Do not read the file (only write it) Do quick statusbar blanking Don't add newlines to the ends of files Don't convert files from DOS/Mac format Don't follow symbolic links, overwrite Don't hard-wrap long lines Don't look at nanorc files Don't show the two help lines Edit a replacement Enable alternate speller Enable smart home key Enable soft line wrapping Enable suspension Enable the use of the mouse End End of Par Enter Enter line number, column number Error deleting lock file %s: %s Error expanding %s: %s Error in %s on line %lu:  Error invoking "%s" Error invoking "sort -f" Error invoking "spell" Error invoking "uniq" Error opening lock file %s: %s Error reading %s: %s Error reading lock file %s: Not enough data read Error writing %s: %s Error writing backup file %s: %s Error writing lock file %s: %s Error writing lock file: Directory '%s' doesn't exist Error writing temp file: %s Execute Command Execute Command Help Text

 This mode allows you to insert the output of a command run by the shell into the current buffer (or a new buffer in multiple file buffer mode).  If you need another blank buffer, do not enter any command.

 The following function keys are available in Execute Command mode:

 Execute external command Exit Exit from nano Exit from the file browser Failed to write backup file, continue saving? (Say N if unsure)  Fatal error: no keys mapped for function "%s".  Exiting.
 File %s is being edited (by %s with %s, PID %d); continue? File Browser Help Text

 The file browser is used to visually browse the directory structure to select a file for reading or writing.  You may use the arrow keys or Page Up/Down to browse through the files, and S or Enter to choose the selected file or enter the selected directory.  To move up one level, select the directory called ".." at the top of the file list.

 The following function keys are available in the file browser:

 File Name to Append to File Name to Prepend to File Name to Write File exists, OVERWRITE ?  File to insert [from %s]  File to insert into new buffer [from %s]  File was modified since you opened it, continue saving ?  File: Finished Finished checking spelling Finished formatting First File First Line Fix Backspace/Delete confusion problem Fix numeric keypad key confusion problem For ncurses: Formatter Forward FullJstify Function '%s' does not exist in menu '%s' Get Help Go To Dir Go To Directory Go To Line Go To Line Help Text

 Enter the line number that you wish to go to and hit Enter.  If there are fewer lines of text than the number you entered, you will be brought to the last line of the file.

 The following function keys are available in Go To Line mode:

 Go To Text Go back one character Go back one word Go forward one character Go forward one word Go just beyond end of paragraph; then of next paragraph Go one screenful down Go one screenful up Go to beginning of current line Go to beginning of paragraph; then of previous paragraph Go to directory Go to end of current line Go to file browser Go to line and column number Go to next line Go to next linter msg Go to previous line Go to previous linter msg Go to the first file in the list Go to the first line of the file Go to the last file in the list Go to the last line of the file Go to the matching bracket Go to the next file in the list Go to the previous file in the list Got 0 parsable lines from command: %s Hard wrapping of overlong lines Help mode Home I can't find my home directory!  Wah! If needed, use nano with the -I option to adjust your nanorc settings.
 If you have selected text with the mark and then search to replace, only matches in the selected text will be replaced.

 The following function keys are available in Search mode:

 If you need another blank buffer, do not enter any filename, or type in a nonexistent filename at the prompt and press Enter.

 The following function keys are available in Insert File mode:

 In Selection:   Indent Text Indent the current line Insert File Help Text

 Type in the name of a file to be inserted into the current file buffer at the current cursor location.

 If you have compiled nano with multiple file buffer support, and enable multiple file buffers with the -F or --multibuffer command line flags, the Meta-F toggle, or a nanorc file, inserting a file will cause it to be loaded into a separate buffer (use Meta-< and > to switch between file buffers).   Insert a newline at the cursor position Insert a tab at the cursor position Insert another file into the current one Insert the next keystroke verbatim Internal error: can't match line %d.  Please save your work. Internal error: cannot set up redo.  Please save your work. Internal error: unknown type.  Please save your work. Invalid line or column number Invoke formatter, if available Invoke the linter, if available Invoke the spell checker, if available Invoking formatter, please wait Invoking linter, please wait Invoking spell checker, please wait Justify Justify the current paragraph Justify the entire file Key invalid in non-multibuffer mode Key name is too short Key name must begin with "^", "M", or "F" Last File Last Line Log & read location of cursor position Log & read search/replace string history Mac Format Main nano help text

 The nano editor is designed to emulate the functionality and ease-of-use of the UW Pico text editor.  There are four main sections of the editor.  The top line shows the program version, the current filename being edited, and whether or not the file has been modified.  Next is the main editor window showing the file being edited.  The status line is the third line from the bottom and shows important messages.   Mark Set Mark Text Mark Unset Mark text starting from the cursor position Missing color name Missing formatter command Missing key name Missing linter command Missing magic string name Missing option Missing regex string Missing syntax name Modified Mouse support Must specify a function to bind the key to Must specify a menu (or "all") in which to bind/unbind the key New Buffer New File Next File Next Line Next Lint Msg Next Page Next Word NextHstory Nn No No Replace No conversion from DOS/Mac format No current search pattern No file name No linter defined for this type of file! No matching bracket No more open file buffers Non-blank characters required Not a bracket Nothing in undo buffer! Nothing to re-do! Option		GNU long option		Meaning
 Option		Meaning
 Option "%s" requires an argument Option is not a valid multibyte string Path %s is not a directory and needs to be.
Nano will be unable to load or save search history or cursor positions.
 Prepend Prepend Selection to File Preserve XON (^Q) and XOFF (^S) keys Prev File Prev Line Prev Lint Msg Prev Page Prev Word PrevHstory Print version information and exit Quoting string Read %lu line Read %lu lines Read %lu line (Converted from DOS and Mac format - Warning: No write permission) Read %lu lines (Converted from DOS and Mac format - Warning: No write permission) Read %lu line (Converted from DOS and Mac format) Read %lu lines (Converted from DOS and Mac format) Read %lu line (Converted from DOS format - Warning: No write permission) Read %lu lines (Converted from DOS format - Warning: No write permission) Read %lu line (Converted from DOS format) Read %lu lines (Converted from DOS format) Read %lu line (Converted from Mac format - Warning: No write permission) Read %lu lines (Converted from Mac format - Warning: No write permission) Read %lu line (Converted from Mac format) Read %lu lines (Converted from Mac format) Read File Read a file into a new buffer by default Reading File Reading file into separate buffer Reading from stdin, ^C to abort
 Recall the next search/replace string Recall the previous search/replace string Received SIGHUP or SIGTERM
 Redid action (%s) Redo Redo the last undone operation Refresh Refresh (redraw) the current screen Regex strings must begin and end with a " character Regexp Repeat the last search Replace Replace a string or a regular expression Replace this instance? Replace with Replaced %lu occurrence Replaced %lu occurrences Requested fill size "%s" is invalid Requested tab size "%s" is invalid Restricted mode Reverse the direction of the search Save backups of existing files Save file under DIFFERENT NAME ?  Save modified buffer (ANSWERING "No" WILL DESTROY CHANGES) ?  Save modified buffer before linting? Scroll Down Scroll Up Scroll by line instead of half-screen Scroll down one line without scrolling the cursor Scroll up one line without scrolling the cursor Search Search Command Help Text

 Enter the words or characters you would like to search for, and then press Enter.  If there is a match for the text you entered, the screen will be updated to the location of the nearest match for the search string.

 The previous search string will be shown in brackets after the search prompt.  Hitting Enter without entering any text will perform the previous search.   Search Wrapped Search for a string Search for a string or a regular expression Set hard-wrapping point at column #cols Set operating directory Set width of a tab to #cols columns Silently ignore startup issues like rc file errors Smart home key Smooth scrolling Soft wrapping of overlong lines Sorry, keystroke "%s" may not be rebound Special thanks to: Spell Check Help Text

 The spell checker checks the spelling of all text in the current file.  When an unknown word is encountered, it is highlighted and a replacement can be edited.  It will then prompt to replace every instance of the given misspelled word in the current file, or, if you have selected text with the mark, in the selected text.

 The following function keys are available in Spell Check mode:

 Spell checking failed: %s Spell checking failed: %s: %s Start at line LINE, column COLUMN Suspend Suspension Switch to the next file buffer Switch to the previous file buffer Switched to %s Syntax "%s" has no color commands Syntax definition to use for coloring Tab Thank you for using nano! The "default" syntax must take no extensions The "none" syntax is reserved The Free Software Foundation The bottom two lines show the most commonly used shortcuts in the editor.

 The notation for shortcuts is as follows: Control-key sequences are notated with a caret (^) symbol and can be entered either by using the Control (Ctrl) key or pressing the Escape (Esc) key twice.  Escape-key sequences are notated with the Meta (M-) symbol and can be entered using either the Esc, Alt, or Meta key depending on your keyboard setup.   The nano text editor This is the only occurrence This message is for unopened file %s, open it in a new buffer? To Bracket To Files To Linter To Spell Toggle appending Toggle backing up of the original file Toggle prepending Toggle the case sensitivity of the search Toggle the use of DOS format Toggle the use of Mac format Toggle the use of a new buffer Toggle the use of regular expressions Too many backup files? Two single-column characters required Type '%s -h' for a list of available options.
 Unable to create directory %s: %s
It is required for saving/loading search history or cursor positions.
 Uncut Text Uncut from the cutbuffer into the current line Undid action (%s) Undo Undo the last operation Unicode Input Unindent Text Unindent the current line Unjustify Unknown Command Unknown option "%s" Usage: nano [OPTIONS] [[+LINE,COLUMN] FILE]...

 Use "fg" to return to nano.
 Use (vim-style) lock files Use bold instead of reverse video text Use of one more line for editing Use one more line for editing Verbatim Verbatim Input View View mode (read-only) Warning: Modifying a file which is not locked, check directory permission? Where Is WhereIs Next Whitespace display Window size is too small for nano...
 Word Count Write File Help Text

 Type the name that you wish to save the current file as and press Enter to save the file.

 If you have selected text with the mark, you will be prompted to save only the selected portion to a separate file.  To reduce the chance of overwriting the current file with just a portion of it, the current filename is not the default in this mode.

 The following function keys are available in Write File mode:

 Write Out Write Selection to File Write the current file to disk Wrote %lu line Wrote %lu lines XOFF ignored, mumble mumble XON ignored, mumble mumble Yes Yy and anyone else we forgot... disabled enable/disable enabled line %ld/%ld (%d%%), col %lu/%lu (%d%%), char %lu/%lu (%d%%) line break line join nano is out of memory! text add text cut text delete text insert text replace text uncut the many translators and the TP version Project-Id-Version: nano-2.4.2
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-12-06 00:22-0500
PO-Revision-Date: 2015-11-15 10:49+0100
Last-Translator: Antonio Ceballos <aceballos@gmail.com>
Language-Team: Spanish <es@tp.org.es>
Language: es
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Lokalize 1.0
 
 Opciones compiladas: 
No se ha escrito el búfer a %s: %s
 
No se ha escrito el búfer: %s
 
Buffer escrito en %s
 
Pulse Intro para continuar
 
Pulsa Intro para continuar iniciando nano.
  (a reemplazar)  (a reemplazar) en la selección  Correo: nano@nano-editor.org	Web: http://www.nano-editor.org  GNU nano, versión %s
  Se dispone de las siguientes teclas de función en el modo Búsqueda:

  [Respaldo]  [Hacia atrás]  [Mayúsc/minúsc]  [Formato DOS]  [Formato Mac]  [ExpReg] No se encontró «%.*s%s» «%s» es un fichero de dispositivo «%s» es un directorio No se encontró «%s» «start=» requiere su «end=» correspondiente %sPalabras: %lu Líneas: %ld Caracteres: %lu  (dir) enorme (más) (dir padre) +LÍNEA,COLUMNA --backupdir=<dir> --fill=<número> --operatingdir=<dir> --quotestr=<cadena> --speller=<programa> --syntax=<cadena> --tabsize=<número> -C <dir> -Q <cadena> -T <número> -Y <cadena> -o <dir> -r <número> -s <programa> TtAa Todas Además, si pulsa dos veces Esc y escribe después un código decimal de tres dígitos entre 000 y 255, introducirá el carácter de valor correspondiente. Dispone de las siguientes pulsaciones en la ventana principal del editor. Las pulsaciones alternativas se muestran entre paréntesis:

 Añadir Añadir selección al fichero El argumento «%s» tiene una " sin terminar Este es el primer mensaje Este es el último mensaje Auto-sangrado Guardar automáticamente al salir, sin preguntar Sangrar automáticamente nuevas líneas Atrás El color de fondo «%s» no puede ser brillante Borrar Respald fich Respaldo de ficheros Ir atrás El marcador de cita «%s» no es válido: %s Regex «%s» incorrecta: %s Ini de pár. Por cortesía de: Texto de ayuda de Ir a Directorio del Navegador

 Introduzca el nombre del directorio por el que desea navegar.

 Si el completado con el tabulador no está inactivo, puede usar la tecla TAB para (intentar) completar automáticamente el nombre del directorio.

 Se dispone de las siguientes teclas de función en el modo Ir a Directorio del Navegador:

 Texto de ayuda de la orden Búsqueda

 Introduzca las palabras o caracteres que desea buscar y pulse Intro. Si hay una coincidencia para el texto introducido, la pantalla se actualizará en el lugar donde esté la coincidencia más cercana de la cadena buscada.

 Se mostrará entre corchetes la cadena de texto de la búsqueda anterior tras el indicador. Si pulsa Intro sin introducir texto repetirá la última búsqueda.

 Ahora se puede desjustificar. No se puede salir de %s en modo restringido No se puede insertar un fichero desde fuera de %s No se puede ascender de directorio No se puede escribir fuera de %s Cancelar Cancelar la función actual Cancelado No se puede añadir una directiva de color sin una orden de sintaxis No se puede añadir una expresión regular para «header» sin una orden «syntax» No se puede añadir un «linter» sin una orden «syntax» No se puede añadir una expresión regular para «magic» sin una orden «syntax» No se puede añadir un «formatter» sin una orden «syntax» El «%s» no es un nombre de función El «%s» no es un nombre de menú No se puede añadir nada a un enlace simbólico ni al principio ni al final con --nofollow activo No es posible desactivar la opción «%s» Mayús/minú Cerrar Cerrar el fichero mostrado / Salir de nano No se ha reconocido el color «%s».
Los válidos son «green», «red», «blue»,
«white», «yellow», «cyan», «magenta» y
«black», con el prefijo opcional «bright»
para los de primer plano. Coloreado de sintaxis No se admite la orden «%s» en el fichero incluido Orden «%s» no reconocida Orden que ejecutar [desde %s] Orden que ejecutar en el nuevo búfer [desde %s]  Muestra constante de la posición del cursor Mostrar constantemente la posición del cursor Conversión de pulsaciones de tabulador a espacios Convertir tabulaciones a espacios al escribir Copiar txt Copiar la línea actual y guardarla en el cutbuffer No se pudo crear una tubería («pipe») No se pudo encontrar la sintaxis «%s» para extender No se pudo crear otro proceso No se pudo obtener el tamaño del búfer de la tubería («pipe») No se pudo determinar el nombre del equipo para el fichero de bloqueo: %s No se pudo determinar mi identidad para el fichero de bloqueo (falló getpwuid()) No se pudo reabrir stdin desde el teclado
 Contar el número de palabras, líneas y caracteres Creando una lista de palabras mal escritas.  Espere, por favor... Posición Cortar txt Cortar desde el cursor al final de línea Cortar desde el cursor hasta el final de línea Cortar la línea actual y guardarla en el cutbuffer Cortado desde el cursor hasta el final de línea CortHastFin DIR: Format DOS Suprimir Borrar el carácter a la izquierda del cursor Borrar el carácter bajo el cursor Detectar límite entre palabras con más exactitud Se detectó un antiguo fichero histórico de nano (%s) que se movió
a la ubicación preferida (%s).
(Consulte las P+F de nano acerca de este cambio.) Se detectó un antiguo fichero histórico de nano (%s) que se intentó mover
a la ubicación preferida (%s) pero se encontró un error: %s Directorio donde guardar ficheros de respaldo Mostrar la posición del cursor Mostrar esta ayuda No leer el fichero (sólo escribirlo) Borrado rápido de la barra de estado No añadir avances de línea al final de los ficheros No convertir los ficheros desde el formato DOS/Mac No seguir enlaces simbólicos; sobreescribirlos No ajustar rígidamente líneas largas No leer los ficheros nanorc No mostrar las dos líneas de ayuda Editar un reemplazamiento Habilitar un corrector ortográfico alternativo Habilitar una tecla de inicio inteligente Habilitar ajuste de línea suave Habilitar suspensión Habilitar el uso del ratón Fin Fin de pár. Intro Introduzca número de línea, número de columna Error al eliminar el fichero de bloqueo «%s»: %s Error al expandir «%s»: %s Error en %s en la línea %lu:  Error al invocar «%s» Error al invocar «sort -f» Error al invocar «spell» Error al invocar «uniq» Error al abrir el fichero de bloqueo «%s»: %s Error leyendo «%s»: %s Error al leer el fichero de bloqueo «%s»: no se han leído suficientes datos Error al escribir «%s»: %s Error al escribir el fichero de respaldo «%s»: %s Error al escribir el fichero de bloqueo «%s»: %s Error al escribir el fichero de bloqueo: el directorio «%s» no existe Error al escribir en el fichero temporal: %s Ejec orden Texto de ayuda de ejecución de órdenes

Este modo le permite insertar la salida de la orden ejecutado en una consola en el búfer actual (o en búfer nuevo en modo multibúfer). Si necesita otro búfer vacío, no introduzca ningún comando.

Las siguientes teclas de función están disponibles en el modo de ejecución de órdenes:

 Ejecutar una orden externa Salir Salir de nano Salir del navegador de ficheros Fallo al escribir el fichero de respaldo, ¿continuar guardándolo? (responda N si no está seguro)  Error fatal: no hay teclas vinculadas a la función «%s».  Terminando.
 Se está editando el fichero «%s» (usuario %s con %s, PID %d); ¿continuar? Texto de ayuda del Navegador de ficheros

El navegador de ficheros se utiliza para navegar visualmente por la estructura del directorio para seleccionar un fichero para lectura o escritura. Puede usar los cursores o Re/Av Pág para navegar por los ficheros y S o Intro para elegir el fichero seleccionado o entrar en el directorio solicitado. Para subir un nivel, seleccione el directorio ".." en la parte superior de la lista de ficheros.

Se dispone de las siguientes teclas de función en el navegador de ficheros:

 Nombre del fichero al que añadir Nombre del fichero al que anteponer Nombre del fichero a escribir El fichero existe, ¿SOBREESCRIBIR?  Fichero que insertar [desde %s]  Fichero que insertar en el nuevo búfer [desde %s]  Se ha modificado el fichero desde que lo abrió, ¿continuar guardándolo?  Fichero: Acabado Revisión de ortografía finalizada Se acabó de aplicar formato Pri. fich. Pri. línea Arreglar el problema de confusión Retroceso/Suprimir Arreglar el problema de confusión del teclado numérico Por ncurses: Arreglador Adelante JustifTodo La función «%s» no existe en el menú «%s» Ver ayuda Ir a dir Ir al directorio Ir a línea Texto de ayuda de Ir a Línea

 Introduzca el número de la línea a la que desea ir y pulse Intro. Si hay menos líneas de texto que el número que ha introducido, el cursor se moverá a la última línea del fichero.

 Se dispone de las siguientes teclas de función en el modo Ir a Línea:

 Ir a texto Ir hacia atrás un carácter Ir hacia atrás una palabra Ir hacia delante un carácter Ir hacia delante una palabra Ir al final del párrafo; después, al del párrafo siguiente Ir una pantalla hacia abajo Ir una pantalla hacia arriba Ir al principio de la línea actual Ir al principio del párrafo; después, al del párrafo anterior Ir a un directorio Ir al final de la línea actual Ir al navegador de ficheros Ir a una línea y columna Ir a la siguiente línea Ir al siguiente mensaje del corrector Ir a la línea anterior Ir al mensaje anterior del corrector Ir al primer fichero de la lista Ir a la primera línea del fichero Ir al último fichero de la lista Ir a la última línea del fichero Ir a la llave correspondiente Ir al siguiente fichero en la lista Ir al fichero anterior en la lista Se han recibido cero líneas analizables de la orden «%s» Ajuste estricto de líneas largas Modo de ayuda Inicio No se encontró el directorio del usuario.  ¡Ay! Si es necesario, use nano con la opción -I para ajustar su configuración nanorc.
 Si ha escogido texto con el marcador y luego hace búsqueda con reemplazo, sólo se modificarán las coincidencias dentro del texto seleccionado.

 Dispone de las siguientes teclas de función en modo Búsqueda:

 Si necesitase otro búfer vacío, no escriba ningún nombre de fichero o en su lugar escriba el nombre de un fichero que no exista y pulse Intro.

 Dispone de las siguientes teclas de función en el modo Insertar Fichero:

 En la selección:  Sangrar txt Sangrar la línea actual Texto de ayuda de Insertar Fichero

 Escriba el nombre del fichero a insertar en el búfer de fichero actual, en la posición actual del cursor.

 Si ha compilado nano con soporte para búfer múltiples y habilita los búfer múltiples con las opciones -F o --multibuffer, con la combinación Meta-F o usando un fichero nanorc, el fichero será insertado en un búfer diferente (use Meta-< y > para cambiar de búfers de fichero).  Insertar un retorno de carro en la posición del cursor Insertar un carácter de tabulación en la posición del cursor Insertar otro fichero en el actual Insertar la próxima pulsación literalmente Error interno: no se puede encontrar la línea %d.  Guarde su trabajo. Error interno: no se puede configurar un rehacer.  Guarde su trabajo. Error interno: tipo desconocido.  Guarde su trabajo. Número de línea o columna no válido Invocar el arreglador (si está disponible) Invocar el corrector de sintaxis (si está disponible) Invocar el corrector ortográfico (si está disponible) Invocando el «formatter».  Espere, por favor... Invocando el corrector de sintaxis.  Espere, por favor... Invocando el corrector ortográfico.  Espere, por favor... Justificar Justificar el párrafo actual Justificar el fichero completo Tecla no válida en modo no-multibúfer El nombre de la combinación es demasiado corto El nombre de una combinación debe comenzar por «^», «M» o «F» Últ. fich. Últ. línea Guardar y restaurar la posición del cursor Registrar y leer histórico de cadenas de búsqueda/reemplazo Format Mac Texto de ayuda principal de nano

 El editor nano está diseñado para emular la funcionalidad y sencillez de uso del editor de texto UW Pico. El editor cuenta con cuatro secciones principales. La línea superior muestra la versión del programa, el nombre del fichero que se está editando, y si ha sido modificado o no. La ventana principal del editor muestra lo que está siendo editado. La línea de estado es la tercera empezando por abajo y muestra mensajes importantes.  Marca establecida Marcar txt Marca borrada Marcar texto desde la posición actual del cursor Falta un nombre de color Falta un nombre de programa detrás de «formatter» Falta el nombre de la combinación Falta un nombre de programa detrás de «linter» Falta el nombre de la cadena mágica Falta una opción Falta la cadena regex Falta el nombre para la sintaxis Modificado Soporte para ratón Debe especificar una función a la cual vincular la tecla Debe especificar un menú (o «all») en el cual vincular/desvincular la tecla Búfer nuevo Nuevo fichero Fich sig Línea sig. Mens. sig. Pág. sig. Palabr sig Sig. hist. Nn No No sustit. Sin conversión desde formato DOS/Mac No hay patrón de búsqueda Sin nombre de fichero No se ha definido un corrector de sintaxis para este tipo de fichero No hay una llave correspondiente No hay más búferes de ficheros abiertos Se precisan caracteres que no sean blancos No es una llave Nada para deshacer en el búfer Nada para rehacer Opción		Opción larga		Significado
 Opción		Significado
 La opción «%s» precisa un argumento La opción no es una cadena multibyte válida La ruta «%s» no es un directorio y debe serlo.
Nano no podrá cargar ni guardar el histórico de búsquedas ni las posiciones del cursor.
 Anteponer Anteponer selección al fichero Conservar teclas XON (^Q) y XOFF (^S) Fich ant Línea ant. Mens. ant. Pág. ant. Palabr ant Prev hist. Mostrar información sobre la versión y salir Marcador de cita %lu línea leída %lu líneas leídas %lu línea leída (convertida desde formatos DOS y Mac; aviso: no hay permiso de escritura) %lu líneas leídas (convertidas desde formatos DOS y Mac; aviso: no hay permiso de escritura) %lu línea leída (convertida desde formatos DOS y Mac) %lu líneas leídas (convertidas desde formatos DOS y Mac) %lu línea leída (convertida desde formato DOS; aviso: no hay permiso de escritura) %lu líneas leídas (convertidas desde formato DOS; aviso: no hay permiso de escritura) %lu línea leída (convertida desde formato DOS) %lu líneas leídas (convertidas desde formato DOS) %lu línea leída (convertida desde formato Mac; aviso: no hay permiso de escritura) %lu líneas leídas (convertidas desde formato Mac; aviso: no hay permiso de escritura) %lu línea leída (convertida desde formato Mac) %lu líneas leídas (convertidas desde formato Mac) Leer fich. Leer fichero dejándolo en un buffer diferente como comportamiento predefinido Leyendo fichero Leer fichero dejándolo en un buffer diferente Leyendo de stdin, ^C para abortar
 Recuperar la siguiente cadena de búsqueda/reemplazo Recuperar la cadena previa de búsqueda/reemplazo Recibido SIGHUP o SIGTERM
 Acción rehecha (%s) Rehacer Rehacer la última operación deshecha Actualizar Redibujar la pantalla actual Las cadenas de regex han de empezar y acabar con un carácter " Exp. reg. Repetir la última búsqueda Reemplazar Reemplazar una cadena o expresión regular ¿Reemplazar esta instancia? Reemplazar con %lu ocurrencia reemplazada %lu ocurrencias reemplazadas El tamaño «%s» de llenado no es válido El tamaño «%s» de tabulador no es válido Modo restringido Cambiar la dirección de búsqueda Guardar respaldo de los ficheros existentes ¿Guardar el fichero con un NOMBRE DIFERENTE?  ¿Guardar el búfer modificado (RESPONDER "No" DESTRUIRÁ LOS CAMBIOS)?  ¿Guardar el búfer modificado antes de invocar el corrector? Despl abaj Despl arri Desplazarse línea a línea en lugar de media pantalla Desplazar el texto una línea abajo sin mover el cursor Desplazar el texto una línea arriba sin mover el cursor Buscar Texto de ayuda de la orden de búsqueda

 Introduzca las palabras o caracteres que desea buscar y pulse Intro. Si hay una coincidencia para el texto introducido, la pantalla se actualizará en el lugar donde esté la coincidencia más cercana de la cadena buscada.

 Se mostrará la cadena de texto de la búsqueda anterior entre corchetes tras el indicador. Si pulsa Intro sin introducir texto repetirá la última búsqueda. Búsqueda recomenzada Buscar una cadena Buscar una cadena o expresión regular Fijar el límite de ajuste en <número> columnas Establecer el directorio de operación Fijar el ancho de tab a <número> columnas Ignorar silenciosamente problemillas de inicio Tecla de inicio inteligente Desplazamiento suave Ajuste suave de líneas largas La combinación «%s» no puede ser revinculada Agradecimientos especiales para: Texto de ayuda del Corrector de ortografía

 El Corrector de ortografía comprueba la ortografía de todo el texto del fichero actual. Cuando se encuentra una palabra desconocida, se la resalta y se puede proporcionar un sustituto. Después preguntará si se quiere reemplazar todas las coincidencias de esa palabra mal escrita en el fichero actual o, si ha seleccionado un texto marcándolo, dentro de la selección.

 Dispone de las siguientes teclas de función en el modo Corrector de ortografía:

 Comprobación de ortografía fallida: %s Comprobación de ortografía fallida: %s: %s Comenzar en LÍNEA, COLUMNA Suspender Suspensión Cambiar al siguiente búfer de fichero Cambiar al búfer de fichero anterior Se ha cambiado a %s La sintaxis «%s» no tiene órdenes de color Definición de sintaxis a usar para coloreado Tab Gracias por usar nano. La sintaxis «default» no toma extensiones La sintaxis «none» está reservada La Free Software Foundation Las dos últimas líneas muestran las combinaciones de teclas usadas más a menudo en el editor.

 La notación para las combinaciones de teclas es como sigue: las secuencias con la tecla Control se indican con un circunflejo (^) y se pueden introducir tanto pulsando la tecla de Control (Ctrl) como pulsando dos veces la de Escape (Esc). Las secuencias con la tecla de Escape se indican con el símbolo Meta (M-) y se pueden introducir con las teclas Esc, Alt o Meta, dependiendo de su configuración de teclado.  El editor de textos GNU nano Ésta es la única coincidencia Este mensaje es para el fichero no abierto %s; ¿abrirlo en un nuevo búfer? A llave A ficheros Corrector Ortografía Conmutar el añadir texto Conmutar la creación de ficheros de respaldo Conmutar el prefijar texto Cambiar la distinción mayús./minús. en la búsqueda Conmutar el uso de formato DOS Conmutar el uso de formato Mac Conmutar el uso de nuevo búfer Cambiar el uso de expresiones regulares ¿Demasiados ficheros de respaldo? Se precisan dos caracteres de una sola columna Teclea «%s -h» para la lista de opciones.
 No se puede crear el directorio «%s»: %s
Se necesita para guardar/cargar el histórico de búsquedas o las posiciones del cursor.
 Pegar txt Pegar el cutbuffer en la línea actual Acción deshecha (%s) Deshacer Deshacer la última operación Entrada de Unicode Desang. txt Quitar sangrado a la línea actual Desjustif. Orden desconocida Opción «%s» desconocida Modo de empleo: nano [OPCIONES] [[+LÍNEA,COLUMNA] FICHERO]...

 Use «fg» para volver a nano.
 Usar ficheros de bloqueo (estilo vim) Usar negrita en lugar de texto inverso Uso de una línea más para editar Usar una línea más para edición Literal Entrada literal Ver Modo de visualización (sólo lectura) Aviso: modificando un fichero no bloqueado; ¿comprobar permisos del dir.? Buscar Siguiente Muestra los blancos La ventana es demasiado pequeña para nano...
 Cont palab Texto de ayuda de Guardar fichero

Escriba el nombre con el que desea guardar el fichero actual y pulse Intro para guardarlo.

Si ha escogido texto marcándolo, se le preguntará si quiere guardar sólo la porción marcada a un fichero diferente. Para reducir la posibilidad de sobreescribir el fichero actual con sólo una parte, el nombre del fichero actual no es el predeterminado en este modo.

Dispone de las siguientes teclas de función en el modo Guardar fichero:

 Guardar Escribir selección al fichero Escribir el fichero actual a disco %lu línea escrita %lu líneas escritas Se ignora XOFF, mmh mmh Se ignora XON, mmh mmh Sí SsYy y cualquiera de quien nos hayamos olvidado... deshabilitado activar/desactivar habilitado línea %ld/%ld (%d%%), col %lu/%lu (%d%%), car %lu/%lu (%d%%) salto de línea unión de líneas nano se ha quedado sin memoria adición de texto corte de texto eliminación de texto inserción de texto reemplazamiento de texto pegado de texto los traductores y el TP versión 