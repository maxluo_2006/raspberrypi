export CROSS_COMPILE=arm-linux-gnueabihf-
export ARCH=arm
export PATH=~tools/arm-bcm2708/gcc-linaro-arm-linux-gnueabihf-raspbian-x64/bin
export INSTALL_MOD_PATH='your rootfs'

Building the kernel:
https://www.raspberrypi.org/documentation/linux/kernel/building.md
Kernel version: linux 4.1 -> branch rpi-4.1.y
Setting up kernel build:
KERNEL=kernel7
make bcm2709_defconfig

make zImage modules dtbs
install the driver modules to the target rootfs:
sudo make modules_install


building u-boot:

setting the build tools:

apt-get update
apt-get install build-essential
apt-get build-dep u-boot

setting the build environment for u-boot:
# current version of U-Boot as of 17.8.2015
make rpi_b_config

//example for compiling with 8 cores host
make -j8 

